package com.wjw.utils;

import java.util.UUID;

public class GUID {
	public static String create(){
		UUID uuid = UUID.randomUUID();
		String guid = uuid.toString().replaceAll("-", "").toUpperCase();
		
		return guid;
	}
	
	public static void main(String[] args){
		for(int i=0; i<5; i++){
			System.out.println(GUID.create());
		}
	}
}