package com.wjw.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author wjw
 * @cTime 2016年3月15日上午11:48:00
 */
public class ListUtil {
	
	/**
	 * 拼接list集合元素，给元素加单引号"'"
	 * @param list
	 * @return
	 */
	public static String getStrSqlByList(List<String> list){
		if(list.size() == 0) return "";
		String str="";
		for(String item:list){
			str+=",'"+item+"'";
		}
		return str.substring(1);
	}
	
	/**
	 * 拼接list集合元素,以逗号分隔
	 * @param list
	 * @return
	 */
	public static String getStrByList(List<String> list){
		if(list.size() == 0) return "";
		String str="";
		for(String item:list){
			str+=","+item+"";
		}
		return str.substring(1);
	}
	
	/**
	 * 以“,”拆分字符串，并给每个元素加单引号"'"再拼装字符串
	 * @param strs
	 * @return
	 */
	public static String getStrSqlByStrs(String strs){
		if(strs.length() == 0) return "";
		String str="";
		String[] obj=strs.split(",");
		for(String item:obj){
			if(item.equals("")){
				continue;
			}
			str+=",'"+item+"'";
		}
		return str.substring(1);
	}
	
	/**
	 * 以“,”拆分字符串，并组装为list集合
	 * @param strs
	 * @return
	 */
	public static List<String> getListByStrs(String strs){
		List<String> list=new ArrayList<String>();
		if(strs.length() == 0) return list;
		String[] obj=strs.split(",");
		list=Arrays.asList(obj);
		return list;
	}
	
	/** 获取两个list的差集 **/
	public static List<String> substractList(List<String> listA, List<String> listB){
		List<String> aClone = new ArrayList<String>(listA);
		List<String> bClone = new ArrayList<String>(listB);
		aClone.removeAll(bClone);
		
		return aClone;
	}
	
	/** 获取两个list的并集 **/
	public static List<String> sumList(List<String> listA, List<String> listB){
		List<String> aClone = new ArrayList<String>(listA);
		List<String> bClone = new ArrayList<String>(listB);
		aClone.addAll(bClone);
		
		return aClone;
	}
	
	/** 获取两个list的交集 **/
	public static List<String> commonList(List<String> listA, List<String> listB){
		List<String> aClone = new ArrayList<String>(listA);
		List<String> bClone = new ArrayList<String>(listB);
		aClone.retainAll(bClone);
		
		return aClone;
	}
	
	public static List<Integer> stringToList (String str,String separ) {
		List<Integer> list=new ArrayList<Integer>();
		StringTokenizer toKenizer = new StringTokenizer(str,separ);
		while (toKenizer.hasMoreElements()) {
			list.add(Integer.valueOf(toKenizer.nextToken()));
		}
		return list;
	} 
	
}