package com.wjw.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

/**
 * @desc 友好时间显示
 * @author wjw
 * @date 2016年11月24日下午1:53:32
 */
public class FriendlyDate {
	
	/**
	（0） 间隔超过一年，显示：xxxx年xx月xx日 xx：xx
	（1） 间隔大于等于二天，显示：xx月xx日 xx：xx
	（2） 间隔一天，显示：昨天 xx：xx
	（3）同一天大于1小时,显示：今天 xx:xx
	（4）一个小时内大于60秒，显示xx分钟前
	（5）60秒内，显示为刚刚
	 */
	public static String formDate(Date date) {  
        Date now = new Date();  
        long result = now.getTime()-date.getTime();
        long nowYes = DateUtils.truncate(now, Calendar.YEAR).getTime(); //当前时间年份
        long nowDay = DateUtils.truncate(now, Calendar.DAY_OF_MONTH).getTime();  //当前时间日期
        long dataDay = DateUtils.truncate(date, Calendar.DAY_OF_MONTH).getTime();  //给定时间日期
        
        if (date.getTime() < nowYes) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);  
        }  
        if ((nowDay - dataDay) >= (2 * 24 * 60 * 60 * 1000)) {  
            return new SimpleDateFormat("MM-dd HH:mm").format(date);  
        }
        if ((nowDay - dataDay) == (24 * 60 * 60 * 1000)) {  
            return new SimpleDateFormat("昨天  HH:mm").format(date);  
        }
        if (result > 60 * 60 * 1000) {  
            return new SimpleDateFormat("今天  HH:mm").format(date);  
        }  
        if (result > 60 * 1000) {  
            return (long) Math.floor((result) * 1d / 60000) + "分钟前";  
        }  
        if (result >= 0) {  
            return "刚刚";  
        }
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);  
    }
	
	public static void main(String[] args) {
		System.out.println(formDate(new Date()));
	}
}

