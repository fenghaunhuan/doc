package com.wjw.utils.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * @className: SignUtil *
 * @description: 签名工具类 使用MD签名
 * @author wjw
 * @cTime 2016年3月16日下午2:10:00
 */
public class SignUtil {
	
    /** 
     * 剔除集合中的空值与sign参数
     * @param sArray 签名集合
     * @return 剔除排序后的集合
     */
    public static Map<String, String> paraFilter(Map<String, String> sArray) {

        Map<String, String> result = new HashMap<String, String>();

        if (sArray == null || sArray.size() <= 0) {
            return result;
        }

        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") || key.equalsIgnoreCase("sign")) {
                continue;
            }
            result.put(key, value);
        }

        return result;
    }

    /** 
     * 按参数名的字典升序排序  
     * @param params 参数集合
     * @return 排序拼接后的参数
     */
    public static String createLinkString(Map<String, String> params) {
    	
		Collection<String> keyset= params.keySet();
		List<String> list = new ArrayList<String>(keyset);
		//对key键值按字典升序排序
		Collections.sort(list);
		String result="";//
        for (int i = 0; i < list.size(); i++) {
            String key = list.get(i);
            String value = params.get(key);

            if (i == list.size() - 1) {//拼接时，不包括最后一个&字符
            	result = result + key + "=" + value;
            } else {
            	result = result + key + "=" + value + "&";
            }
        }
        return result;
    }
    
    /**
     * 验证签名
     * @param ai
     */
	public static Boolean checkSign(Map<String, String> params,String sign){
		String paramStr=SignUtil.createLinkString(SignUtil.paraFilter(params));
		String signStr=Digest.md5Hex(paramStr, "签名秘钥");
		if(StringUtils.isBlank(sign) || !sign.equalsIgnoreCase(signStr)){//签名是否有效
			return false;
		}
		return true;
	}
}