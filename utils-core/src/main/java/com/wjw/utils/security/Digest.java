package com.wjw.utils.security;

import java.io.UnsupportedEncodingException;
import java.security.SignatureException;

import org.apache.commons.codec.digest.DigestUtils;


/**
 * @className: Digest *
 * @description: 摘要工具类
 * @author wjw
 * @date 2016年3月16日 下午2:30:26 *
 */

public class Digest {
	
	final static String input_charset="UTF-8";//默认编码

	 /**
     * MD5加密字符串
     * @return 加密结果
     */
    public static String md5Hex(String text) {
        return DigestUtils.md5Hex(getContentBytes(text, input_charset));
    }
	 /**
     * MD5加密字符串
     * @param text 字符串
     * @param key 密钥
     * @return 加密结果
     */
    public static String md5Hex(String text, String key) {
    	text = text +"&key="+ key;
        return DigestUtils.md5Hex(getContentBytes(text, input_charset));
    }
	 /**
     * MD5加密字符串
     * @param text 字符串
     * @param key 密钥
     * @param input_charset 编码格式
     * @return 加密结果
     */
    public static String md5Hex(String text, String key, String input_charset) {
    	text = text +"&key="+ key;
        return DigestUtils.md5Hex(getContentBytes(text, input_charset));
    }
    
	 /**
     * SHA加密字符串
     * @param text 字符串
     * @return 加密结果
     */
    public static String sha256Hex(String text) {
        return DigestUtils.sha256Hex(getContentBytes(text, input_charset));
    }
	 /**
     * SHA加密字符串
     * @param text 字符串
     * @param key 密钥
     * @param input_charset 编码格式
     * @return 加密结果
     */
    public static String sha256Hex(String text, String key) {
    	text = text + key;
        return DigestUtils.sha256Hex(getContentBytes(text, input_charset));
    }
	 /**
     * SHA加密字符串
     * @param text 字符串
     * @param key 密钥
     * @param input_charset 编码格式
     * @return 加密结果
     */
    public static String sha256Hex(String text, String key, String input_charset) {
    	text = text + key;
        return DigestUtils.sha256Hex(getContentBytes(text, input_charset));
    }
    
    /**
     * 使用给定的 charset 将此 String 编码到 byte 序列，并将结果存储到新的 byte 数组中
     * @param content 需要编码的字符集
     * @param charset 字符集编码格式
     * @return
     * @throws SignatureException
     * @throws UnsupportedEncodingException 
     */
    private static byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("指定的编码集不对,您目前指定的编码集是:" + charset);
        }
    }
    
    public static void main(String[] args) {
    	String text= md5Hex("123456@#.&!a");
    	String text2= md5Hex("123456","@#.&!a");
    	System.out.println(text);
    	System.out.println(text2);
    	
    	String s=Digest.md5Hex("123");
    	System.out.println(s);
	}

}