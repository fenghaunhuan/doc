package com.wjw.utils;

public class NumberUtil {
	
    public static void main(String[] args) {
    	NumberUtil test = new NumberUtil();
        System.out.println(test.lpad(8, 23456L));
    }

    /**
     * 补齐不足长度
     * @param i 长度
     * @param number 数字
     * @return
     */
    public static String lpad(int i, Long number) {
        String f = "%0" + i + "d";
        return String.format(f, number);
    }

}
