package com.wjw.utils;

import bsh.EvalError;
import bsh.Interpreter;

public class BshUtils {
	
	public static void main(String[] args) {
		try {
			//构造一个解释器
			Interpreter interpreter = new Interpreter();
			//对公式求值，并得到结果
				interpreter.eval("sal = 12*2+3-4");
				Integer num= (Integer) interpreter.get("sal");
			//打印输出结果
			System.out.println("结果："+num);
		} catch (EvalError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
