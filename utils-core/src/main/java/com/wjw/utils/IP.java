package com.wjw.utils;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @className IP
 * @description IP工具类
 * @author wjw
 * @cTime 2016年3月19日下午5:59:41
 */
public final class IP {
	
	private static Logger log = LoggerFactory.getLogger(IP.class);

	private IP() {}

	/**
	 * 获取客户端IP地址.<br>
	 * 支持多级反向代理
	 * @param request
	 *            HttpServletRequest
	 * @return 客户端真实IP地址
	 */
	public static String getRemoteAddr(final HttpServletRequest request) {
		try {
			String remoteAddr = request.getHeader("X-Forwarded-For");
			// 如果通过多级反向代理，X-Forwarded-For的值不止一个，而是一串用逗号分隔的IP值，此时取X-Forwarded-For中第一个非unknown的有效IP字符串
			if (isEffective(remoteAddr) && (remoteAddr.indexOf(",") > -1)) {
				String[] array = remoteAddr.split(",");
				for (String element : array) {
					if (isEffective(element)) {
						remoteAddr = element;
						break;
					}
				}
			}
			if (!isEffective(remoteAddr)) {
				remoteAddr = request.getHeader("X-Real-IP");
			}
			if (!isEffective(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
			return remoteAddr;
		} catch (Exception e) {
			log.error("get romote ip error,error message:" + e.getMessage());
			return "";
		}
	}

	/**
	 * 获取客户端源端口
	 * @param request
	 * @return
	 */
	public static int getRemotePort(final HttpServletRequest request) {
		try {
			String port = request.getHeader("remote-port");
			if (isNotEmptyString(port)) {
				try {
					return Integer.parseInt(port);
				} catch (NumberFormatException ex) {
					log.error("convert port to long error , port: " + port);
					return 80;
				}
			} else {
				return 80;
			}
		} catch (Exception e) {
			log.error("get romote port error,error message:" + e.getMessage());
			return 80;
		}
	}

	/**
	 * 远程地址是否有效.
	 * @param remoteAddr 远程地址
	 * @return true代表远程地址有效，false代表远程地址无效
	 */
	private static boolean isEffective(final String remoteAddr) {
		boolean isEffective = false;
		if ((null != remoteAddr) && (!"".equals(remoteAddr.trim()))
				&& (!"unknown".equalsIgnoreCase(remoteAddr.trim()))) {
			isEffective = true;
		}
		return isEffective;
	}
	
	/**
	 * 是否不为空字符串
	 * @param str
	 * @return true表示不是是空字符串和null，false表示是空字符串或null
	 */
	private static boolean isNotEmptyString(String str){
		boolean flag=false;
		if(str!=null&&!"".equals(str)){
			flag=str.trim().isEmpty();
		}
		return flag;
	}
}