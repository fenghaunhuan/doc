package com.wjw.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.StringUtils;

/**
 * @className DateUtil
 * @description 日期时间操作类
 * @author wjw
 * @cTime 2016年3月19日下午5:58:10
 */
public final class DateUtil {
	
	public final static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	public final static String DEFAULT_TIME_FORMAT = "HH:mm:ss";
	public final static String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	/** 格式化字符串为Date类型  */
	public static Date parse(String strDate) {
		if (strDate == null || strDate.trim().equals("")){
			return null;
		}
		return parse(strDate,DEFAULT_DATE_TIME_FORMAT);
	}

	/** 格式化字符串为Date类型 */
	public static Date parse(String date,String format) {
		Date result=null;
		if (date == null || date.trim().equals("")){
			return null;
		}
		SimpleDateFormat dtFormat = null;
		try {
			dtFormat = new SimpleDateFormat(format);
			result=dtFormat.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/** 将日期格式化成yyyy-MM-dd HH:mm:ss */
	public static String formatDate(Date date){
		if(date==null){
			return null;
		}
		return formatDate(date,DEFAULT_DATE_TIME_FORMAT);
	}
	
	/** 将日期格式化成指定的格式 */
	public static String formatDate(Date date,String format) {
		if (date == null){
			return "";
		}
		return new SimpleDateFormat(format).format(date);
	}

	/** 给一个日期增加天数  */
	public static Date addDay(Date date,int add_day){
		Calendar cal=Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR,1);
		return cal.getTime();
	}
	
	/** 给一个日期增加月数 */
	public static Date addMonth(Date date,int add_month) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH,add_month);
		return cal.getTime();
	}
	
	/** 根据年月计算当月的最大天数  */
	public static int getMaxDayByYearMonth(int year, int month) {
		int maxDay = 0;
		int day = 1;
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month - 1, day);
		maxDay = calendar.getActualMaximum(Calendar.DATE);
		return maxDay;
	}
	
	/** 判断是否是日期类型  */
	public static boolean isDate(String dateStr){
		boolean flag=false;
		if(StringUtils.isBlank(dateStr)){
			return false;
		}
		try{
			flag=parse(dateStr,"yyyy-MM-dd")!=null;
		}catch(Exception e){
			e.printStackTrace();
		}
		return flag;
	}
	
	/** 获得系统时间是多少周 */
	public static int getweeks(){
		Calendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		int week = cal.get(Calendar.WEEK_OF_YEAR);
		return week;
	}
	
	/** 获得系统当前时间 */
	public static String getDate(){
		String date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		return date;
	}
	
	/** 获得系统当前年份 */
	public static Integer getYEAR(){
		Calendar a=Calendar.getInstance();
		Integer YEAR=a.get(Calendar.YEAR);
		return YEAR;
	}
	
	/** 获得系统当前月份 */
	public static Integer getMONTH(){
		Calendar a=Calendar.getInstance();
		Integer MONTH=a.get(Calendar.MONTH)+1;
		return MONTH;
	}
	
	/** 获得系统在当月中的天数 */
	public static Integer getDAY(){
		Calendar a=Calendar.getInstance();
		Integer DAY=a.get(Calendar.DAY_OF_MONTH);
		return DAY;
	}
}