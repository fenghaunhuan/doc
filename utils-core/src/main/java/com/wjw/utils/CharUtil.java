package com.wjw.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * 字符工具类
 * @作者 zsf
 * @时间 2013-6-18
 */
public final class CharUtil {
	
	private CharUtil(){}
	
	/**
	 * 转换编码 ,将oldCharType转换到newCharType
	 * @param oldCharType    要转换的编码,例ISO-8859-1
	 * @param newCharType    转换成的编码,例UTF-8
	 * @param text  需要转换的字符串
	 * @return    转换后的字符串
	 */
	public static String oddCharToNewChar(String oldCharType,String newCharType,String text){
		if(text==null){
			return text;
		}
		String result="";
		try {
			result=new String(text.getBytes(oldCharType),newCharType);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 将URL中含有的中文转换为ascii;
	 * 例："我 爱 你" 转换为 "%E6%88%91%20%E7%88%B1%20%E4%BD%A0"
	 * utf8URL编码
	 * @param text
	 * @return
	 */
	public static String utf8UrlEncode(String text){
		StringBuffer result=new StringBuffer();
		for(int i=0;i<text.length();i++){
			char c=text.charAt(i);
			if(c>=0 && c<=255)
				result.append(c);
			else{
				byte[] b=new byte[0];
				try {
					b=Character.toString(c).getBytes("UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				for(int j=0;j<b.length;j++){
					int k=b[j];
					if(k<0)
						k+=256;
					result.append("%"+Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return result.toString().replaceAll(" ","%20");		
	}
	
	/**
	 * 将URL的中乱码(ascii码)转换为中文的URL
	 * 例: "%E6%88%91%20%E7%88%B1%20%E4%BD%A0"转换为"我 爱 你"
	 * @param text
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String utf8UrlDecoder(String text) throws UnsupportedEncodingException{
		return URLDecoder.decode(text, "utf-8");
	}
	
	/**
	 * 将ASCII(数字型的字符串)转换为字符(char)
	 * @param numStr
	 * @return
	 */
	public static char asciiToChar(String numStr) {
		return (char) Integer.parseInt(numStr);
	}
	
	/**
	 * 将字符(char型)转换为ASCII码(数字类型的字符串)
	 * @param code
	 * @return
	 */
	public static String charToAscii(char code) {
		return "" + (int) code;
	}
	public static void main(String[] args) {
		String s="abc"+File.separator+"dd";
		System.out.println(s);
		System.out.println(s.replace("\\", "%20"));
	}
}
