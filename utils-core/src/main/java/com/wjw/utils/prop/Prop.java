package com.wjw.utils.prop;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;


/**
 * @createTime 2013-12-17
 * @author jfinal
 */
public class Prop {
	/**
	 * 默认编码
	 */
	public static final String DEFAULT_ENCODING = "utf-8";
	
	private Properties properties = null;
	
	public Prop(String fileName) {
		this(fileName, DEFAULT_ENCODING);
	}

	/**
	 * 加载多个properties
	 * @param files
	 */
	public Prop(String... files) {
		InputStream is = null;
		properties = new Properties();
		for (String file : files) {
			try {
				is = Thread.currentThread().getContextClassLoader().getResourceAsStream(file);
				if (is == null) {
					throw new IllegalArgumentException("Properties file not found in classpath: " + file);
				}
				properties.load(new InputStreamReader(is, DEFAULT_ENCODING));
			} catch (IOException e) {
				throw new RuntimeException("Error loading properties file.", e);
			} finally {
				if (is != null)
					try {is.close();} catch (IOException e) {e.printStackTrace();}
			}
		}
	}
	
	public Prop(String fileName, String encoding) {
		InputStream inputStream = null;
		try {
			inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
			if (inputStream == null)
				throw new IllegalArgumentException("Properties file not found in classpath: " + fileName);
			properties = new Properties();
			properties.load(new InputStreamReader(inputStream, encoding));
		} catch (IOException e) {
			throw new RuntimeException("Error loading properties file.", e);
		}
		finally {
			if (inputStream != null) try {inputStream.close();} catch (IOException e) {e.printStackTrace();}
		}
	}
	
	public Prop(File file) {
		this(file, DEFAULT_ENCODING);
	}
	
	public Prop(File file, String encoding) {
		if (file == null)
			throw new IllegalArgumentException("File can not be null.");
		if (file.isFile() == false)
			throw new IllegalArgumentException("Not a file : " + file.getName());
		
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(file);
			properties = new Properties();
			properties.load(new InputStreamReader(inputStream, encoding));
		} catch (IOException e) {
			throw new RuntimeException("Error loading properties file.", e);
		}
		finally {
			if (inputStream != null) try {inputStream.close();} catch (IOException e) {e.printStackTrace();}
		}
	}
	
	
	public String get(String key) {
		return properties.getProperty(key);
	}
	
	public String get(String key, String defaultValue) {
		String value = get(key);
		return (value != null) ? value : defaultValue;
	}
	
	public Integer getInt(String key) {
		String value = get(key);
		return (value != null) ? Integer.parseInt(value) : null;
	}
	
	public Integer getInt(String key, Integer defaultValue) {
		String value = get(key);
		return (value != null) ? Integer.parseInt(value) : defaultValue;
	}
	
	public Long getLong(String key) {
		String value = get(key);
		return (value != null) ? Long.parseLong(value) : null;
	}
	
	public Long getLong(String key, Long defaultValue) {
		String value = get(key);
		return (value != null) ? Long.parseLong(value) : defaultValue;
	}
	
	public Boolean getBoolean(String key) {
		String value = get(key);
		return (value != null) ? Boolean.parseBoolean(value) : null;
	}
	
	public Boolean getBoolean(String key, Boolean defaultValue) {
		String value = get(key);
		return (value != null) ? Boolean.parseBoolean(value) : defaultValue;
	}
	
	public boolean containsKey(String key) {
		return properties.containsKey(key);
	}
	
	public Properties getProperties() {
		return properties;
	}
}
