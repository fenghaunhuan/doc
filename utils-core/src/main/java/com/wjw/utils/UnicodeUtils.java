package com.wjw.utils;

public class UnicodeUtils {

	
    /**
     * 十六进制Unicode字符串  转换为  中文
     * @param code
     * @return
     */
    private static String UnicodeTostring(String code) {
        StringBuffer sb = new StringBuffer(code);  
        int pos;  
        while ((pos = sb.indexOf("\\u")) > -1) {  
            String tmp = sb.substring(pos, pos + 6);  
            sb.replace(pos, pos + 6, Character.toString((char) Integer  
                    .parseInt(tmp.substring(2), 16)));  
        }  
        return code = sb.toString();  
    } 
    /**
     * 中文  转换为 十六进制Unicode字符串
     * @param code
     * @return
     */
    public static String stringToUnicode(String s) {
    	String str = "";
    	for (int i = 0; i < s.length(); i++) {
    	int ch = (int) s.charAt(i);
    	if (ch > 255)
    	str += "\\u" + Integer.toHexString(ch);
    	else
    	str += "\\" + Integer.toHexString(ch);
    	}
    	return str;
    }

	public static void main(String[] args) {
		System.out.println(UnicodeTostring("\u66f4\u65b0|\u521b\u5efa|\u9a8c\u8bc1\u6570\u636e\u5e93\u8868\u7ed3\u6784|\u4e0d\u4f5c\u6539\u53d8     \u9ed8\u8ba4update(create,validate,none)"));
		System.out.println(stringToUnicode("成员不存在或删除失败"));
	}
}
