package com.wjw.utils.sms;

import java.io.Serializable;
import java.util.Date;
import java.util.Random;

/**
 * @desc 短信实体类
 * @author wjw
 * @date 2016年11月24日下午5:40:21
 */
public class Sms implements Serializable {
    
	private static final long serialVersionUID = 8768777386634592157L;
	
	public static String defaultTemplate = "您的确认码是：%s。请不要吧确认码泄露给他人";// 默认模板
	
	private static final String charset = "0123456789";// 随机因子
	private static final int codelen = 6;
	
	/** 手机号 */
    private String mobile;
    
    /** 内容 */
    private String content;
    
    /** code */
    private String code;
    
    /** 发送者 */
    private String fromUser;
    
    /** 接收者 */
    private String toUser;
    
    /** 发送时间 */
    private Date sendTime;
    
    public Sms() {
		// TODO Auto-generated constructor stub
	}
    /**
     * 短信构造器
     * @param mobile 手机号
     * @param content 内容
     * @param code  验证码
     * @param fromUser 来自
     * @param toUser  发送到
     */
    public Sms(String mobile,String content,String code,String fromUser,String toUser) {
		// TODO Auto-generated constructor stub
    	this.mobile=mobile;
    	this.content=content;
    	this.code=code;
    	this.fromUser=fromUser;
    	this.toUser=toUser;
	}
    
	/** 随机生成6位纯数字的验证码 */
	public static String createCode() {// 生成验证码
		int len = charset.length() - 1;
		String result = "";
		for (int i = 0; i < codelen; i++) {
			char[] arrs = charset.toCharArray();
			Random r = new Random();
			result += arrs[r.nextInt(len)];
		}
		return result;
	}
	
    @Override
    public String toString() {
        return "Sms [mobile=" + mobile + ", content=" + content + ", fromUser=" + fromUser + ", toUser=" + toUser + ",sendTime="+sendTime+"]";
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
}
