package com.wjw.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @desc 注册用户名是检测用户名工具类
 * @author wjw
 * @date 2016年11月18日下午6:20:14
 */
public class UserNameCheck {
	
	/**
	 * 判断是否包含中文
	 * @Title: ICrashConfigService 
	 * @Description: 判断是否包含中文
	 * @param str
	 * @return
	 */
	public static boolean isContainsChinese(String str){
		String regEx = "[\u4e00-\u9fa5]";
		Pattern pat = Pattern.compile(regEx);
		Matcher matcher = pat.matcher(str);
		boolean flag = false;
		if (matcher.find()){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * 判断字符串是否以字母开头4-16字符
	 * @Title: ICrashConfigService 
	 * @Description: 判断字符串是否以字母开头4-16字符
	 * @param c
	 * @return
	 */
	public static boolean isUserName(String c){
		c = new String(c.getBytes());
		String pattern ="[a-zA-Z][a-zA-Z0-9]{3,15}";//author by jlq
		Pattern p = Pattern.compile(pattern);
		Matcher result = p.matcher(c);
		return result.find();
	}
}