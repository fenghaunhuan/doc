package com.wjw.utils.office;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @desc Excel输出工具类
 * @author wjw
 * @date 2016年11月23日下午6:52:32
 */
public final class ExcelOutUtil {

    /**
     * 日志对象
     */
    protected static Logger log = LoggerFactory.getLogger(ExcelOutUtil.class);
	
    /**
     * 下载Excel
     * @param list
     * @param response
     * @param fileName
     */
	public void outExcel(List<Object> list,HttpServletResponse response,String fileName){
		
		response.setHeader("Content-Disposition", "attachment; filename="+fileName);
		response.setContentType("application/ms-download;");
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		//获取参数个数作为excel列数
		int columeCount = 10;
		//获取List size作为excel行数
		int rowCount = list.size();
		HSSFSheet sheet = workbook.createSheet(fileName);
		//创建第一栏
		HSSFRow headRow = sheet.createRow(0);
		String[] titleArray = {"报修单号","报修类型", "报修描述","报修时间","报修楼(栋)/房","报修人","手机号" ,"报修状态","评价等级","评价类容"};
		for(int m=0;m<=columeCount-1;m++){
			HSSFCell cell = headRow.createCell(m);
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			sheet.setColumnWidth(m, 6000);
			HSSFCellStyle style = workbook.createCellStyle();
			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			short color = HSSFColor.RED.index;
			font.setColor(color);
			style.setFont(font);
			//填写数据
			cell.setCellStyle(style);
			cell.setCellValue(titleArray[m]);
		}
		int index = 0;
		
		//写入数据
		for(Object item : list){
			HSSFRow row = sheet.createRow(index+1);
			for(int n=0;n<=columeCount-1;n++){
				row.createCell(n);
				row.getCell(0).setCellValue("");
				row.getCell(1).setCellValue("");
				row.getCell(2).setCellValue("");
			}
			index++;
		}
		//写到输出流上
		try {
			BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream());
			workbook.write(output);
			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}