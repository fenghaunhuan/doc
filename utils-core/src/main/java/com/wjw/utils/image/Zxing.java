package com.wjw.utils.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;

/**
 * @className Zxing
 * @description 条形码  or 二维码
 * @author wjw
 * @cTime 2016年4月25日上午9:47:19
 */
public class Zxing {
	
    /**
     * 日志对象
     */
    protected static Logger log = LoggerFactory.getLogger(Zxing.class);
    
	/**
	 * 生成bitMatrix
	 * @param text  二维码内容
	 * @param width 宽度
	 * @param height 高度
	 */
	public static BitMatrix getBitMatrix(String text,int width,int height){
	    Hashtable<EncodeHintType, String> hints= new Hashtable<EncodeHintType, String>();   
	    hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
	    BitMatrix bitMatrix=null;
	    try {
	    	bitMatrix= new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, width, height,hints);
		} catch (WriterException e) {
			e.printStackTrace();
		} 
		return bitMatrix;  	
	}
	
	/** 生成图片  */
	public static void toImg(BitMatrix data,String imagesPath,String suffix){
	    Hashtable<EncodeHintType, String> hints= new Hashtable<EncodeHintType, String>();   
	    hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
	    File file=new File(imagesPath);
		try {
			MatrixToImageWriter.writeToPath(data, suffix, file.toPath());
		} catch (IOException e) {
			log.error("io异常");
			e.printStackTrace();
		}  
	}
	
	 /** 
     * 解析二维码 or 条形码
     */  
    public static Result decode(String filePath) {  
    	 Result result = null;
        try {  
        	BufferedImage image = ImageIO.read(new File(filePath));  
            LuminanceSource source = new BufferedImageLuminanceSource(image);
            Binarizer binarizer = new HybridBinarizer(source);  
            BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);  
            Map<DecodeHintType, Object> hints = new HashMap<DecodeHintType, Object>();  
            hints.put(DecodeHintType.CHARACTER_SET, "UTF-8");  
            result = new MultiFormatReader().decode(binaryBitmap, hints);// 对图像进行解码 
        } catch (IOException e) {  
            e.printStackTrace();  
        } catch (NotFoundException e) {  
            e.printStackTrace();  
        }  
        return result;
    }  
    
	/**
	 * 通过矩阵数据参数安卓矩阵对象
	 */
//	public static Bitmap getBitmap(BitMatrix bitMatrix){
//		// 开始利用二维码数据创建Bitmap图片，分别设为黑白两色  
//        int w = bitMatrix.getWidth();  
//        int h = bitMatrix.getHeight();  
//        int[] data = new int[w * h];  
//        for (int y = 0; y < h; y++) {  
//            for (int x = 0; x < w; x++) {  
//                if (bitMatrix.get(x, y))  
//                    data[y * w + x] = 0xff000000;// 黑色  
//                else  
//                    data[y * w + x] = -1;// -1 相当于0xffffffff 白色  
//            }  
//        }
//  
//        // 创建一张bitmap图片，采用最高的效果显示  
//        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);  
//        // 将上面的二维码颜色数组传入，生成图片颜色  
//        bitmap.setPixels(data, 0, w, 0, 0, w, h);  		
//	}
	
	public static void main(String[] args) {
//		1.生成二维码
//		BitMatrix data=getBitMatrix("18613375360", 100, 100);
//		try {
//			toImg(data, "D:\\5.png","png");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		2.解析二维码
//		 Result result =decode("D:\\5.png");
//		 if(result.getBarcodeFormat().toString().equalsIgnoreCase("QR_CODE")){
//			 //二维码
//         	System.out.println("二维码："+result.getText());
//         }else{
//        	 //条形码
//        	 JSONObject content = JSONObject.parseObject(result.getText());  
//             System.out.println("author： " + content.getString("author"));  
//             System.out.println("zxing：  " + content.getString("zxing"));  
//         }
	}
	
}
