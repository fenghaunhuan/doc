package com.wjw.utils.image;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @desc 图片合成工具
 * @author wjw
 * @date 2016年11月24日下午5:11:21
 */
public class BImgKit {
	
    /**
     * 日志对象
     */
    protected static Logger log = LoggerFactory.getLogger(BImgKit.class);
    
	/**
     * 图片设置圆角
     * @param srcImage
     * @param radius
     * @param border
     * @param padding
     * @return
     * @throws IOException
     */
    public static BufferedImage setRadius(BufferedImage srcImage, int radius, int border, int padding) throws IOException{
        int width = srcImage.getWidth();
        int height = srcImage.getHeight();
        int canvasWidth = width + padding * 2;
        int canvasHeight = height + padding * 2;
        
        BufferedImage image = new BufferedImage(canvasWidth, canvasHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gs = image.createGraphics();
        gs.setComposite(AlphaComposite.Src);
        gs.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gs.setColor(Color.WHITE);
        gs.fill(new RoundRectangle2D.Float(0, 0, canvasWidth, canvasHeight, radius, radius));
        gs.setComposite(AlphaComposite.SrcAtop);
        gs.drawImage(setClip(srcImage, radius), padding, padding, null);
        if(border !=0){
            gs.setColor(Color.GRAY);
            gs.setStroke(new BasicStroke(border));
            gs.drawRoundRect(padding, padding, canvasWidth - 2 * padding, canvasHeight - 2 * padding, radius, radius);    
        }
        gs.dispose();
        return image;
    }
    
    /**
     * 图片设置圆角
     * @param srcImage
     * @return
     * @throws IOException
     */
    public static BufferedImage setRadius(BufferedImage srcImage) throws IOException{
        int radius = (srcImage.getWidth() + srcImage.getHeight()) / 6;
        return setRadius(srcImage, radius, 0, 10);
    }
    
    /**
     * 图片切圆角
     * @param srcImage
     * @param radius
     * @return
     */
    public static BufferedImage setClip(BufferedImage srcImage, int radius){
        int width = srcImage.getWidth();
        int height = srcImage.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gs = image.createGraphics();

        gs.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gs.setClip(new RoundRectangle2D.Double(0, 0, width, height, radius, radius));
        gs.drawImage(srcImage, 0, 0, null);
        gs.dispose();
        return image;
    }
    
    /**
     * 图片按比类别压缩并切圆角
     * @param srcImage
     * @param 缩放图片大小，高宽相同
     * @return图片像素
     */
    public static int[] saveImgAndClip(BufferedImage srcImage,int size){
    	BufferedImage zoom = new BufferedImage(size, size,BufferedImage.TYPE_INT_RGB);// 新生成结果图片0
    	zoom.getGraphics().drawImage(srcImage.getScaledInstance(size,size,java.awt.Image.SCALE_SMOOTH), 0, 0, null);
    	
    	BufferedImage result = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gs = result.createGraphics();
        gs.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gs.setClip(new RoundRectangle2D.Double(0, 0, size, size, 15,15));
        gs.drawImage(zoom, 0, 0, null);
        gs.dispose();
        int[] RGBArry=new int[size*size];//从图片中读取RGB
        RGBArry= result.getRGB(0,0,size,size,RGBArry,0,size);
        return RGBArry;
    }
    
    /**
    * 圆角处理
    * @param BufferedImage
    * @param cornerRadius
    * */
    public static String makeRoundedCorner(String srcImageFile, String result, String type, int cornerRadius) {
        try {
            BufferedImage image = ImageIO.read(new File(srcImageFile));
            int w = image.getWidth();
            int h = image.getHeight();
            BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = output.createGraphics();
            output = g2.getDeviceConfiguration().createCompatibleImage(w, h, Transparency.TRANSLUCENT);
            g2.dispose();
            g2 = output.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.fillRoundRect(0, 0,w, h, cornerRadius, cornerRadius);
            g2.setComposite(AlphaComposite.SrcIn);
            g2.drawImage(image, 0, 0, w, h, null);
            g2.dispose();
            ImageIO.write(output, type, new File(result));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /** java蒙版切圆角，消除java代码切圆角出现锯齿现象 */
    public static String makeRoundedCorner2(String src, String dis, String type) {
        try {
            //读取原图
            //蒙版
            BufferedImage disImage = ImageIO.read(new File(dis));
            //素材原图
            BufferedImage srcImage = ImageIO.read(new File(src));
            int w = disImage.getWidth();
            int h = disImage.getHeight();
            Graphics2D  g2 = disImage.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setComposite(AlphaComposite.SrcIn);
            g2.drawImage(srcImage, 0, 0, w, h, null);
            g2.dispose();
            ImageIO.write(disImage, type, new File(src));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    
    public static void main(String[] args) {
//    	图片合成test
    	
		try {
			int[] item1 = saveImgAndClip(ImageIO.read(new File("图片绝对地址1")), 100);
			int[] item2 = saveImgAndClip(ImageIO.read(new File("图片绝对地址2")), 100);
			int[] item3 = saveImgAndClip(ImageIO.read(new File("图片绝对地址3")), 100);
			// 创建一张透明背景图
			BufferedImage result = new BufferedImage(450, 450, BufferedImage.TYPE_INT_ARGB);
			result.setRGB(0, 0, 0, 0, item1, 0, 100);//将第一张图设置到左上角
			result.setRGB(0, 0, 350, 0, item2, 0, 100);//将第二张图设置到右上角
			result.setRGB(0, 0, 0, 350, item3, 0, 100);//将第三张图设置到左下角
		} catch (IOException e) {
			e.printStackTrace();
			log.error("io异常");
		}
	}
}
