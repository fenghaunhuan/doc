/******************************************************************************
 * Copyright (C) 2013 ShenZhen ComTop Information Technology Co.,Ltd
 * All Rights Reserved.
 * 本软件为深圳乐牛开发研制。未经本公司正式书面同意，其他任何个人、团体不得使用、
 * 复制、修改或发布本软件.
 ******************************************************************************/

package com.wjw.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 序列化、反序列化 工具类
 * 
 * @author macong
 * @since 1.0
 * @version 2013-12-16 macong
 */
public class SerializeUtils {
    
    /** FIXME */
    private final static Logger LOG = LoggerFactory.getLogger(SerializeUtils.class);
    
    /**
     * 序列化
     * 
     * @param object 需要序列化的对象
     * @return 序列化后的字节
     */
    public static byte[] serialize(Object object) {
        ObjectOutputStream oos = null;
        ByteArrayOutputStream baos = null;
        try {
            // 序列化
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            byte[] bytes = baos.toByteArray();
            return bytes;
        } catch (Exception e) {
            LOG.error("序列化异常", e);
        }
        return null;
    }
    
    /**
     * 反序列化
     * 
     * @param bytes 字节
     * @return 反序列化后的对象
     */
    public static Object deserialize(byte[] bytes) {
        ByteArrayInputStream bais = null;
        try {
            // 反序列化
            bais = new ByteArrayInputStream(bytes);
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (Exception e) {
            LOG.error("反序列化异常", e);
        }
        return null;
    }
    
}
