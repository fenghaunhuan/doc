package com.wjw.utils.database.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @className DbUtil
 * @description db操作类
 * @author wjw
 * @cTime 2016年3月19日下午5:58:20
 */
public class DbUtil {
	
	Statement stat;
	Connection conn;//连接对象
	
	/**
	 * 构造函数 ：文件不存在则创建并打开到数据库的连接
 	*/
	public DbUtil(String url){
		try {
			Class.forName("org.sqlite.JDBC");//加载驱动
			conn = DriverManager.getConnection(url);//建立连接不存在则创建文件
			stat = conn.createStatement();
		} catch (ClassNotFoundException e) {
			System.err.println("加载驱动失败！");
		} catch (SQLException e) {
			System.err.println("连接数据库失败！");
			e.printStackTrace();
		}
	}
	
	/**
	 * 判断将要创建的表是否存在。  
	 */
	public Boolean tabExists(String tableName){
		Boolean b=false;
		try {
			stat.executeQuery("SELECT * FROM "+tableName +" LIMIT 0,1");
			b=true;
		} catch (SQLException e) {
			b=false;
		} //判断表是否存在
		return b;
	}
	
	/**
	 * 创建表 存在则删除重新创建
	 * @param tabName 
	 * @param sql
	 */
   public Boolean createTable(String tabName,String sql){
	  Boolean b=false;
	  try {
		stat.executeUpdate("drop table if exists "+tabName+";");//存在则删除
		stat.executeUpdate(sql);
		b=true;
	  } catch (SQLException e) {
		b=false;
		e.printStackTrace();
	  }
	  return b;
	}
	
	/**
	 * 数据查询操作1：select语句中指定查询条件的参数
	 */
  public ResultSet queryDB(String sql){ 
	  ResultSet rs=null;
	try {
		rs = stat.executeQuery(sql);
	} catch (SQLException e) {
		e.printStackTrace();
	} // 查询数据
	  return rs;
  }
  
  /**
   * 增， 删 ，改 操作
   * @param sql
   * @return
   */
  public Boolean executeUpd(String sql){
	  Boolean b=false;
	  try {
		stat.executeUpdate(sql);
		b=true;
	} catch (SQLException e) {
		b=false;
		try {
			conn.rollback();//操作失败则回滚数据库
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		e.printStackTrace();
	}
	  return true;
  }
  
  

  /**
   * 增， 删 ，改 操作2
   * @param sql
   * @param paramArray 参数值
   */
  public Boolean executeWithParams(String sql,List<String> paramArray){
	  boolean b=false;
	  try {
//			创建执行对象
			PreparedStatement prep = conn.prepareStatement(sql);
			int i=1;
			for(String temp:paramArray){
				prep.setString(i,temp);
				i++;
			}
			//4、执行sql语句
			b=prep.execute();
			prep.close();
		} catch (SQLException e) {
			try {
				conn.rollback();//操作失败则回滚数据库
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	   return b;
  }
  
  /**
   * 关闭数据库
   * @return
   */
  public Boolean closeDb(){
	  boolean b=false;
	  try {
		conn.close();
		b=true;
	} catch (SQLException e) {
		b=false;
		e.printStackTrace();
	}
	  return b;
  }
  
  public static void main(String[] args) {
	try {
		DbUtil obj=new DbUtil("jdbc:sqlite://D:/test.db");
		// 2 创建一个表tbl1，录入数据
		ResultSet rs = obj.stat.executeQuery("select * from test;"); // 查询数据
		while (rs.next()) { // 将查询到的数据打印出来
			System.out.print("id = " + rs.getInt("id") + ", "); // 列属性一
			System.out.println("name = " + rs.getString("name")); // 列属性二
		}
		rs.close();//关闭结果集
		obj.closeDb(); // 结束数据库的连接
	} catch (Exception e) {
		e.printStackTrace();
	}
  }
}