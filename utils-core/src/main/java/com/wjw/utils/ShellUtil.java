package com.wjw.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @desc shell工具类
 * @author wjw
 * @date 2016年11月18日下午6:20:14
 */
public class ShellUtil {
	
	/**
	 * @Title: runShell
	 * @Description: 执行shell脚本，非线程
	 * @param command  执行命令
	 * @return 执行结果
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static String runShell(String command) throws IOException, InterruptedException{
		Process proc = Runtime.getRuntime().exec(command);
		InputStream is = proc.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"));
		StringBuffer sb = new StringBuffer();
		String line = "";
		while((line = reader.readLine())!= null){
			sb.append(line);
		}
		proc.waitFor();
		is.close();
		reader.close();
		proc.destroy();
		
		return sb.toString();
	}
}