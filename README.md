## 最轻api接口管理系统java版
**介绍**

doc是一个轻量级api文档管理系统，在JFinal最新版3.1基础上搭建的一个maven Java项目,重要的事说三遍：轻，轻，轻。让您轻松管理自己的api接口文档，也可以在这个系统的基础上开发你自己的项目。

**菜单功能**

- 【文档管理】：项目管理、项目目录、项目文档、文档版本
- 【系统管理】：菜单管理、账户管理、角色管理、权限管理

**maven依赖**
- jfinal-core 1.0.0     ：对jfinal和jfinal-ext还有权限模块，用户、角色、菜单包装
- utils-core 1.0.0      ：工具类

**框架及插件**

后端框架:JFinal 3.1, Shiro 1.2.3, Ehcache

前端插件：angularjs-1.4.3,  layer2.1, markdown-0.3.3编辑器, zTree-3.5.22,  zui-1.5.0

发布后 **doc.war** 才13M左右，非常小，js插件部分引用百度cdn库，部分引用七牛cdn库。

![输入图片说明](https://git.oschina.net/uploads/images/2017/0716/134848_d319e37e_476441.png "QQ拼音截图20170716124357.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0716/134901_9599d1d4_476441.png "QQ拼音截图20170716124418.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0716/135037_d5e480e9_476441.png "QQ拼音截图20170716124456.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0716/134913_7f259a87_476441.png "QQ拼音截图20170716124723.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0716/134933_07f4b79e_476441.png "QQ拼音截图20170716124611.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0716/135054_7449f64c_476441.png "QQ拼音截图20170716124621.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0716/135104_893e374e_476441.png "QQ拼音截图20170716124645.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0716/135114_30a67caa_476441.png "QQ拼音截图20170716124645.png")
源码：http://git.oschina.net/wjw1234/doc

## 演示地址

1.前台 http://xzysoft.com/doc/index.do

2.后台 http://xzysoft.com/doc

3.用户名/密码：demo/123

有好的建议和想法都可以反馈:59834156@qq.com