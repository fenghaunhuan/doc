package com.wjwframework.jfinal.verify;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.core.Controller;
import com.wjw.utils.RegExpUtil;


/**
 * @className commonVerify
 * @description 基础校验类
 * @author wjw
 * @cTime 2016年4月6日上午10:15:58
 */
public class CommonVerify extends BaseVerify {
	
	protected void validate(Controller controller) {
		//phone存在则判断是否合法
		String phone = controller.getPara("phone");
		if(StringUtils.isNotBlank(phone) && !RegExpUtil.isMobile(phone)){
    		addError("手机号码格式不正确!");
    	}
		
		//seq存在则判断是否合法
		String seq = controller.getPara("seq");
		if(StringUtils.isNotBlank(seq) && !RegExpUtil.isNumeric(seq)){
    		addError("排序值不是整型!");
    	}
		
		//state存在则判断是否合法
		String state = controller.getPara("state");
		if(StringUtils.isNotBlank(state) && !RegExpUtil.isNumeric(state)){
    		addError("state不是整型!");
    	}
		
		//邮箱存在则判断是合法
		String mail = controller.getPara("user.mail");
		if(StringUtils.isNotBlank(mail) && !RegExpUtil.isMail(mail)){
    		addError("邮箱格式不正确!");
    	}
		
	}

}
