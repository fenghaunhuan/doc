package com.wjwframework.jfinal.verify;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;
import com.wjwframework.jfinal.vo.Response;

/**
 * @className BaseVerify
 * @description 
 * @author wjw
 * @cTime 2016年3月17日下午4:00:33
 */
public abstract class BaseVerify extends Validator {
	
	public Response response=new Response();//响应对象 
	
    protected void addError(String errorMsg){
        invalid = true;
        List<String> msgList=(List<String>) response.result.get("msgList");
        if(msgList==null){
        	msgList=new ArrayList<String>();
        }
        msgList.add(errorMsg);
        response.result.put("msgList", msgList);
        return;
    }
    protected abstract void validate(Controller controller1);
    
	protected void handleError(Controller controller) {
		response.setErrcode(2);//参数类型不正确
		response.setMsg("参数格式错误");
		List<String> msgList=(List<String>) response.result.get("msgList");
		response.setMsg(msgList.get(msgList.size()-1));
		controller.renderJson(response);
	}

}
