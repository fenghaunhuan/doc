package com.wjwframework.jfinal.config;

import com.jfinal.config.Constants;
import com.jfinal.config.JFinalConfig;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log;
import com.wjwframework.jfinal.web.utils.WebUtils;

public abstract class BaseConfig extends JFinalConfig {
	
	private static final Log log = Log.getLog(BaseConfig.class);
	
	public static Prop baseProp;
	
	public void init(Constants me, String basePath){
		//step 1.加载参数配置
		baseProp=PropKit.use(basePath);//加载基本配置
		//step 2.获取环境参数
		WebUtils.webAppRealPath = PathKit.getWebRootPath().replace("\\", "/");
		WebUtils.webAppContextPath = WebUtils.webAppRealPath.substring(WebUtils.webAppRealPath.lastIndexOf("/"));
		
		log.debug("项目物理路径："+WebUtils.webAppRealPath);		// 例如： D:/java/eclipse4.4/apache-tomcat-7.0.76/webapps/reman
		log.debug("项目上下文路径："+WebUtils.webAppContextPath);  //例如：/reman
		
	}
	
}