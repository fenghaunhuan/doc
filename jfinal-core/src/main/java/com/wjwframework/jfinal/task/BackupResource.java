package com.wjwframework.jfinal.task;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackupResource  implements Job{
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		backRes();
	}
	
	/* 静态资源路径 */
	private static String tomcatPath="D:\\java\\eclipse4.4_2\\apache-tomcat-7.0.57\\webapps\\wb\\resource\\images";
	private static String workPath="D:\\java\\eclipse4.4_2\\workspace\\wb\\WebContent\\resource\\images";
	
	/**
	 * 拷贝静态资源
	 */
	public void backRes(){
		log.info("----------开始同步静态资源begin------------");
		//1.遍历工作目录将文件名存储到集合中
		File[] file=new File(workPath).listFiles();
		List<String> nameList=new ArrayList<String>();
		for(int i=0;i<file.length;i++){
			String fileName=file[i].getName();
			nameList.add(fileName);
		}
		//2.遍历tomcat,将资源拷贝进工作空间
		File[] resFile=new File(tomcatPath).listFiles();
		for(int i=0;i<resFile.length;i++){
			String fileName=resFile[i].getName();
			//如果该文件不存在工作空间则拷贝文件到工作空间
			if(!nameList.contains(fileName)){
				String filePath=workPath+"\\"+fileName;
				try {
					FileUtils.copyFile(resFile[i],new File(filePath));
				} catch (IOException e) {
					e.printStackTrace();
					log.error("拷贝文件异常！");
				}
			}
		}
		log.info("----------静态资源同步完成end！------------");
	}
}