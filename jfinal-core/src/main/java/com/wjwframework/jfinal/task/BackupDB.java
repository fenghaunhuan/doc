package com.wjwframework.jfinal.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wjw.utils.database.MysqlCMD;
import com.wjwframework.jfinal.config.BaseConfig;

public class BackupDB  implements Job{
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	private static String host;
	private static String username;
	private static String password;
	private static String mysqldumpPath; //mysqldump程序路径
	private static String database;//数据库名称
	private static String backupPath;//*备份目录
	private static boolean isBackup; //是否备份
	
	static{
		host=BaseConfig.baseProp.get("host");
		username=BaseConfig.baseProp.get("username");
		password=BaseConfig.baseProp.get("password");
		mysqldumpPath=BaseConfig.baseProp.get("mysqldumpPath");
		backupPath=BaseConfig.baseProp.get("backupPath");
		isBackup=BaseConfig.baseProp.getBoolean("isBackup");
		database=BaseConfig.baseProp.get("database");
	}
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		MysqlCMD mysqlCmd = new MysqlCMD(username, password, mysqldumpPath, host, backupPath, isBackup, database);
		mysqlCmd.backup();//执行备份命令
	}
}