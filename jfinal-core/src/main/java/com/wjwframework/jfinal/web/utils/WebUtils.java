package com.wjwframework.jfinal.web.utils;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.wjwframework.jfinal.web.constant.Constants;
import com.wjwframework.jfinal.web.model.SysUser;

/**
 * @desc 系统工具类
 * @author wjw
 * @time 2017年7月11日下午5:20:55
 */
public abstract class WebUtils {
    
    /** 默认编码 UTF-8 */
    public static final String DEFAULT_CHARSET = "UTF-8";
    
    /** web 应用上下文路径 */
    public static volatile String webAppContextPath = null;
    
    /** web 应用真实路径 */
    public static volatile String webAppRealPath = null;
    
    private static ThreadLocal tl = new ThreadLocal<HttpServletRequest>();
    
	public static void set(HttpServletRequest rquest) {
		tl.set(rquest);
	}
	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) tl.get();
	}
	public static void remove() {
		tl.remove();
	}
     
    /**
     * web 应用上下文路径(例如：/unifyedu)
     */
    public static String getWebRoot() {
        return webAppContextPath;
    }
    
    /**
     * web 应用真实路径
     */
    public static String getWebRealPath() {
        return webAppRealPath;
    }
	
    /**
	 * 获取session中user对象
	 */
	public static final SysUser getUser() {
		HttpSession session = getRequest().getSession();
		SysUser sysUser = (SysUser) session.getAttribute(Constants.SESSION_USER_KEY);
        return sysUser;
	}
	
	/**
	 * 获取request中的参数值
	 */
	public static String getParameter(String field) {
		HttpServletRequest request = getRequest();
		return request.getParameter(field);
	}
	
    /**
     * 使用默认的UTF-8字符集解码请求参数值。
     */
    public static String decode(String value) {
        return decode(value, DEFAULT_CHARSET);
    }
    
    /**
     * 使用默认的UTF-8字符集编码请求参数值
     */
    public static String encode(String value) {
        return encode(value, DEFAULT_CHARSET);
    }
    
    /**
     * 使用指定的字符集解码请求参数值。
     */
    public static String decode(String value, String charset) {
        String result = null;
        if (!StringUtils.isEmpty(value)) {
            try {
                result = URLDecoder.decode(value, charset);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return result;
    }
    
    /**
     * 使用指定的字符集编码请求参数值。
     */
    public static String encode(String value, String charset) {
        String result = null;
        if (!StringUtils.isEmpty(value)) {
            try {
                result = URLEncoder.encode(value, charset);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return result;
    }
    
    /**
     * 从URL中提取所有的参数。
     */
    public static Map<String, String> splitUrlQuery(String query) {
        Map<String, String> result = new HashMap<String, String>();
        
        String[] pairs = query.split("&");
        if (pairs != null && pairs.length > 0) {
            for (String pair : pairs) {
                String[] param = pair.split("=", 2);
                if (param != null && param.length == 2) {
                    result.put(param[0], param[1]);
                }
            }
        }
        
        return result;
    }
}
