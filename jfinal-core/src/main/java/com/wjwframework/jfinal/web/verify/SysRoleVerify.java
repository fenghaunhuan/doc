package com.wjwframework.jfinal.web.verify;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.core.Controller;
import com.wjw.utils.RegExpUtil;
import com.wjwframework.jfinal.verify.BaseVerify;


/**
 * @className RoleVerify
 * @description role校验类
 * @author wjw
 * @cTime 2016年3月17日下午6:35:46
 */
public class SysRoleVerify extends BaseVerify {
	
	protected void validate(Controller controller) {
		//seq存在则判断是否合法
		String seq = controller.getPara("role.seq");
		if(StringUtils.isNotBlank(seq) && !RegExpUtil.isNumeric(seq)){
    		addError("排序值为数字!");
    	}
	}

}
