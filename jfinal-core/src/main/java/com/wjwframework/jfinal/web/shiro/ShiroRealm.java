package com.wjwframework.jfinal.web.shiro;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.Subject;

import com.jfinal.plugin.activerecord.Record;
import com.wjw.utils.IP;
import com.wjwframework.jfinal.web.constant.Constants;
import com.wjwframework.jfinal.web.model.SysRolePower;
import com.wjwframework.jfinal.web.model.SysUser;
import com.wjwframework.jfinal.web.utils.WebUtils;

public class ShiroRealm extends AuthorizingRealm {
	
	private SysUser userDao = SysUser.dao;
	private SysRolePower rpDao = SysRolePower.dao;
	

	/**
	 * 登录认证回调函数,登录时调用
	 */
	protected AuthenticationInfo doGetAuthenticationInfo(
		AuthenticationToken authcToken) throws AuthenticationException {
		UserToken token = (UserToken) authcToken;
		SysUser sysUser=userDao.login(token.getPrincipal());
		if(sysUser==null){					//帐号不存在
			throw new UnknownAccountException();
		}else if(!sysUser.getPassword().equals(token.getPrincipal().getPassword())){//密码不正确
			throw new IncorrectCredentialsException();
		}else if(sysUser.getInt("state")==2){			//用户未激活或被锁定
			throw new LockedAccountException();
		}
		//将用户保存至session
		Subject subject=SecurityUtils.getSubject();
		subject.getSession().setAttribute(Constants.SESSION_USER_KEY,sysUser);//将用户保存至session
		//将权限对象集合添加到session中
		List<Record> powerList=rpDao.findPowersByRoleId(sysUser.getRoleId(),sysUser.getStr("coding"));
		subject.getSession().setAttribute(Constants.SESSION_AUTH_POWER, powerList);//将用户菜单对象添加到session中
		//更新用户登录信息
		String id=(String) subject.getSession().getId();
		String host=IP.getRemoteAddr(WebUtils.getRequest());
		userDao.getCommDbUtil().updateSet("sid=?,last_ip=?,last_time=?","id=?",id,host,new Date(),sysUser.getId());
		//将用户信息存入shiro
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
         		sysUser, //用户对象
         		sysUser.getPassword(), //密码
                getName()  //realm name
         );
		return authenticationInfo;
	}
	/**
	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
	 */       
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		SysUser sysUser  = (SysUser) principals.getPrimaryPrincipal();//获取登录用户对象
		if (sysUser != null) {
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			info.addRole(sysUser.getStr("coding"));//添加角色编码
			
			List<String> permissionList=new ArrayList<String>();
			for (Record obj : ShiroUtils.getPowers()){//获取拥有的权限并添加到权限系统中
				permissionList.add(obj.getStr("permission"));
			}
			info.addStringPermissions(permissionList);//添加权限代码集合
			return info;
		} else {
			return null;
		}
	}

	/**
	 * 更新用户授权信息缓存.
	 */
	public void clearCachedAuthorizationInfo(String principal) {
		SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
		clearCachedAuthorizationInfo(principals);
	}

	/**
	 * 清除所有用户授权信息缓存.
	 */
	public void clearAllCachedAuthorizationInfo() {
		Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
		if (cache != null) {
			for (Object key : cache.keys()) {
				cache.remove(key);
			}
		}
	}

}