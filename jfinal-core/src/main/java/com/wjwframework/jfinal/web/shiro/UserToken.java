package com.wjwframework.jfinal.web.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

import com.wjwframework.jfinal.web.model.SysUser;



public class UserToken extends UsernamePasswordToken {

	private static final long serialVersionUID = 6787344269886036610L;

	private SysUser sysUser;
	
	public UserToken() {
		super();
	}
	public UserToken(SysUser sysUser,boolean rememberMe) {
		super(sysUser.getStr("name"),sysUser.getStr("password"),rememberMe);
		this.sysUser = sysUser;
	}
	@Override
	public String getUsername() {
		return sysUser.getUserName();
	}
	@Override
	public char[] getPassword() {
		return sysUser.getPassword().toCharArray();
	}
	@Override
	public SysUser getPrincipal() {
		return sysUser;
	}
	@Override
	public Object getCredentials() {
		return sysUser.getPassword();
	}
}
