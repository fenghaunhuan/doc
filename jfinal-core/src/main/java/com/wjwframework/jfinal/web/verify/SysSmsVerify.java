package com.wjwframework.jfinal.web.verify;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.core.Controller;
import com.wjw.utils.RegExpUtil;
import com.wjwframework.jfinal.verify.BaseVerify;

/**
 * @className PowerVerify
 * @description power校验类
 * @author wjw
 * @cTime 2016年3月18日上午9:40:49
 */
public class SysSmsVerify extends BaseVerify {
	
	protected void validate(Controller controller) {
		//seq存在则判断是否合法
		String seq = controller.getPara("power.seq");
		if(StringUtils.isNotBlank(seq) && !RegExpUtil.isNumeric(seq)){
    		addError("排序值为数字!");
    	}
	}

}
