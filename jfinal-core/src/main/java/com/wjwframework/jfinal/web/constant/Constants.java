package com.wjwframework.jfinal.web.constant;

public interface Constants {
	/**
	 * session中的用户key
	 */
	public static final String SESSION_USER_KEY = "SESSION_USER_KEY";
	
	/**
	 * session中的用户权限代码
	 */
	public static final String SESSION_AUTH_POWER = "SESSION_AUTH_POWER";
	
	/**
	 * 默认编码
	 */
	public static final String DEFAULT_ENCODING = "utf-8";

	public static final String DATE_FORMAT="yyyy-MM-dd";
	
	public static final String DATE_TIME_FORMAT="yyyy-MM-dd HH:mm:ss";
	
}
