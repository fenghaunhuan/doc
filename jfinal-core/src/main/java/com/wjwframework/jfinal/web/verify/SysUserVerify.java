package com.wjwframework.jfinal.web.verify;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.core.Controller;
import com.wjw.utils.RegExpUtil;
import com.wjwframework.jfinal.verify.BaseVerify;


/**
 * @className UserVerify
 * @description user校验类
 * @author wjw
 * @cTime 2016年3月17日下午3:38:32
 */
public class SysUserVerify extends BaseVerify {
	
	protected void validate(Controller controller) {
		//seq存在则判断是否合法
		String seq = controller.getPara("user.seq");
		if(StringUtils.isNotBlank(seq) && !RegExpUtil.isNumeric(seq)){
    		addError("排序值为数字!");
    	}
		//邮箱存在则判断是合法
		String mail = controller.getPara("user.mail");
		if(StringUtils.isNotBlank(mail) && !RegExpUtil.isMail(mail)){
    		addError("邮箱格式不正确!");
    	}
		//phone存在则判断是否合法
		String phone = controller.getPara("user.phone");
		if(StringUtils.isNotBlank(phone) && !RegExpUtil.isMobile(phone)){
    		addError("手机号码格式不正确!");
    	}
	}

}
