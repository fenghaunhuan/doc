package com.wjwframework.jfinal.model;

import com.wjwframework.jfinal.vo.PageNavation;

public interface BaseModelImpl{
	
	/**
	 * 获取
	 * @return
	 */
	CommonDbUtil getCommDbUtil();
	/**
	 * 根据id集合删除对应集合对象
	 * @param ids
	 * @return boolean
	 */
	public boolean deleteByIds(String ids);//根据id集合删除对象
	/**
	 * 传入PageNavation分页对象查询数据集合返回Page<Model>分页对象
	 * @param PageNavation
	 * @return Page<Model>
	 */
	public PageNavation findList(PageNavation page);//查询对象集合,并获取该条件下集合数量

}
