package com.wjwframework.jfinal.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wjw
 * @time 2014年6月30日下午11:46:46
 */
public class PageNavation<T> implements Serializable {
	
	private static final long serialVersionUID = -6074371198315626282L;
	
	private int startIndex=0;					// 开始查询位置
	private int pageNow=1;						// 当前页数，默认为1
	private int pageSize=15;					// 每页显示的数量
	private int totalPage;						// 总页数
	private int totalRow;						// 总记录数
	private String order="desc";				// 排序类型 默认降序
	private String sort;						// 排序字段
	private String search;						// 搜索内容
    private Map<String,Object> paramMap;		//Map存储参数
    
    private List<T> data = new ArrayList<T>();//存储分页对象集合
    private int pageListSize = 9;				// 页码列表大小，默认9 
    private List<Integer> pageNumList = new ArrayList<Integer>();
	
	public PageNavation() {
		
	}
	/**
	 * 分页对象构造器
	 * @param paramMap
	 */
	public PageNavation(Map<String, String> paramMap) {
		if(paramMap.get("search")!=null){
			this.search=paramMap.get("search").toString();//搜索关键字
		}
		if(paramMap.get("sort")!=null){
			this.sort=paramMap.get("sort").toString();//排序字段
		}
		if(paramMap.get("order")!=null){
			this.order=paramMap.get("order").toString();//排序方式
		}
		if(paramMap.get("pageSize")!=null){
			this.pageSize=Integer.parseInt(paramMap.get("pageSize").toString());//获取每页显示数量
		}
		if(paramMap.get("pageNow")!=null){
			this.pageNow=Integer.parseInt(paramMap.get("pageNow").toString());//设置当前页码
		}
	}
	
	public int getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
	public int getPageNow() {
		return pageNow;
	}
	public void setPageNow(int pageNow) {
		this.pageNow = pageNow;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public int getTotalRow() {
		return totalRow;
	}
	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}
	public Map<String, Object> getParamMap() {
		return paramMap;
	}
	public void setParamMap(Map<String, Object> paramMap) {
		Map<String, Object> map=new HashMap<String, Object>();
		for(String key:paramMap.keySet()){
			map.put(key, paramMap.get(key));
		}
		this.paramMap = map;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	
	public int getPageListSize() {
		return pageListSize;
	}
	public void setPageListSize(int pageListSize) {
		this.pageListSize = pageListSize;
	}
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	public void setPageNumList(List<Integer> pageNumList) {
		this.pageNumList = pageNumList;
	}
	
	// 得到页面列表  
    public List<Integer> getPageNumList() {  
        this.pageNumList.removeAll(this.pageNumList);// 设置之前先清空  
        if (this.totalPage > this.pageListSize) {
            int halfSize = this.pageListSize / 2;  
            int first = 1;  
            int end = 1;  
            if (this.pageNow - halfSize < 1) { // 当前页靠近最小数1  
                first = 1;  
                end = this.pageListSize;  
            } else if (this.totalPage - this.pageNow < halfSize) { // 当前页靠近最大数  
                first = this.totalPage - this.pageListSize + 1;  
                end = this.totalPage;  
            } else {  
                first = this.pageNow - halfSize;  
                end = this.pageNow + halfSize;  
            }  
            for (int i = first; i <= end; i++) {  
                this.pageNumList.add(i);  
            }  
        } else {  
            for (int i = 0; i < this.totalPage; i++) {  
                this.pageNumList.add(i + 1);  
            }  
        }  
        return pageNumList;  
    }
    
}
