package com.wjwframework.jfinal.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

/**
 * JSON返回对象
 * @author wjw
 * @time 2016年1月7日下午4:58:05
 */
public class Response extends HashMap<String,Object> implements Serializable {

	private static final long serialVersionUID = -2941004975419422778L;

	public Map<String,Object> result=null;
	
	protected int errcode;
	protected String msg;
	
	public Response(){
		this.errcode=0;
		this.msg="";
		this.result=new HashMap<String,Object>();
		put("errcode",errcode);
		put("msg",msg);
		put("result",result);
	}
	
	public Response(int errcode){
		this();
		this.errcode=errcode;
		put("errcode",errcode);
	}
	
	public Response(int errcode,String msg){
		this();
		this.errcode=errcode;
		this.msg=msg == null?"":msg;
		put("errcode",errcode);
		put("msg",msg);
	}


	public Integer getErrcode() {
		return errcode;
	}
	
	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}

	public void setErrcode(int errcode) {
		this.errcode = errcode;
		put("errcode",errcode);
	}

	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
		put("msg",msg);
	}
	
	/* 
	 * 实现链式调用
	 */
	@Override
	public Response put(String key, Object value) {
		super.put(key, value);
		// TODO Auto-generated method stub
		return this;
	}

	public static void main(String[] args) {
		Response r = new Response();
		r.result.put("user", "ss");
		System.out.println(JSONObject.toJSONString(r));
	}
}
