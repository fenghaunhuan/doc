package test;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.wjw.utils.security.Digest;
import com.wjw.utils.security.SignUtil;

public class HttpTest {
	
	protected static Prop prop = PropKit.use("config/base.properties");// 加载基本配置;
	
	public static void main(String[] args) {
		Map<String, String> param = new HashMap<String, String>();//参数
	    param.put("page","1");
	    param.put("page_size","20");
	    param.put("u_id","8890E961A6B94A0AA4F4B8E900743178");
	    param.put("u_token","1A18892DB0004A379A068D611CF9B83D");
	    param.put("search","");
	    
//	    page=1  page_size=20  search=  u_id=8890E961A6B94A0AA4F4B8E900743178  u_token=1A18892DB0004A379A068D611CF9B83D  sign=5c97ee41026f376d3eb5dfe0bedc435d  

	     
		String paramStr=SignUtil.createLinkString(SignUtil.paraFilter(param));
		System.out.println(paramStr);
		String sign=Digest.md5Hex(paramStr, prop.get("md5.key"));
//	    param.put("sign",sign);
	    System.out.println("签名结果："+sign);
//	    String data = HttpUtils.params(param);
//		Map<String, String> headers = new HashMap<String, String>();//请求头参数
//		headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
//		String response = HttpKit.post("http://192.168.0.47/wb/api/topic/add.do", data, headers);
//		System.out.println(response);
//		
////		sendImg();
//		Long d=new Date().getTime();
//		Long d1=1441578050932L;
//		System.out.println(d1);
//		Date date=DateUtil.parse(d1);
//		System.out.println(DateUtil.formatDate(date, "yyyy-MM-dd HH:mm:ss SSS"));
	}
	
    public static String params(Map<String, String> paramsMap) {
        String params="";
    	for(String key: paramsMap.keySet()){
        	String value=paramsMap.get(key);
        	params = params + "&"+key + "=" + value;
        }
        return params.substring(1);
    }

    public static void sendImg(){
    	// 只包含字符串参数时默认使用BodyParamsEntity，
    	// 类似于UrlEncodedFormEntity（"application/x-www-form-urlencoded"）。
    	// 加入文件参数后默认使用MultipartEntity（"multipart/form-data"），
    	// 如需"multipart/related"，xUtils中提供的MultipartEntity支持设置subType为"related"。
    	// 使用params.setBodyEntity(httpEntity)可设置更多类型的HttpEntity（如：
    	// MultipartEntity,BodyParamsEntity,FileUploadEntity,InputStreamUploadEntity,StringEntity）。
    	// 例如发送json参数：params.setBodyEntity(new StringEntity(jsonStr,charset));
//    	RequestParams params = new RequestParams();
//    	params.setContentType("multipart/form-data");
//    	params.addHeader("u_id", "19FC92FDDF104DFD9A455B467D5FC180");
//    	params.addHeader("u_token", "02CA43181A4F475C949AB9ED0C61529D");
////    	params.addQueryStringParameter("name", "value");
//    	params.addBodyParameter("file", new File("D:\\微人_v01.jpg"));
//    	
//    	HttpUtils http = new HttpUtils();
//    	http.send(HttpRequest.HttpMethod.POST,"http://192.168.0.47/wb/info/add.do",params,new RequestCallBack<String>() {
//	        @Override
//	        public void onStart() {
//	        	System.out.println("conn...开始上传...");
//	        }
//	        @Override
//	        public void onLoading(long total, long current, boolean isUploading) {
//	            if (isUploading) {
//	            	System.out.println("upload: " + current + "/" + total);
//	            } else {
//	            	System.out.println("reply: " + current + "/" + total);
//	            }
//	        }
//	        @Override
//	        public void onSuccess(ResponseInfo<String> responseInfo) {
////	        	System.out.println("reply: " + responseInfo.result);
//	        }
//	        @Override
//	        public void onFailure(HttpException error, String msg) {
//	        	System.out.println(error.getExceptionCode() + ":" + msg);
//	        }
//    	});
    }
}
