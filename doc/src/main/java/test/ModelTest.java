package test;
 
import com.doc.config._MappingKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
 
/**
 * @author ilgqh
 * JFinal的Model测试用例
 * 
 */
public class ModelTest{
     
    /**
     * 启动ActiveRecordPlugin插件
     */
    static{
    	Prop jdbcProp=PropKit.use("config/jdbc.properties");//加载数据库配置文件
    	C3p0Plugin c3p0Plugin = new C3p0Plugin(jdbcProp.get("jdbcUrl"), jdbcProp.get("username"), jdbcProp.get("password").trim());
    	// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setShowSql(true);
		// 所有配置在 MappingKit 中搞定
		_MappingKit.mapping(arp);
		//开启ActiveRecordPlugin
		c3p0Plugin.start();
		arp.start();
    }

    public static void main(String[] args) {
//    	List<SysUser> userlist=SysUser.dao.find("select * from sys_user");
//    	for(SysUser u:userlist){
//    		System.out.println(u.getUserName());
//    	}
	}
 
}