package com.doc.config;

import org.apache.log4j.Logger;

import com.doc.controller.DocController;
import com.doc.controller.ProjectController;
import com.doc.controller.TemplateController;
import com.doc.controller.sys.MainController;
import com.doc.controller.sys.PowerController;
import com.doc.controller.sys.RoleController;
import com.doc.controller.sys.UserController;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.ext.plugin.shiro.ShiroPlugin;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.SqlReporter;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.wjwframework.jfinal.config.BaseConfig;
import com.wjwframework.jfinal.handler.RequestHandler;
import com.wjwframework.jfinal.interceptor.ExceptionInterceptor;

public class Config extends BaseConfig {
	
	public static String basePath="config/base.txt";
	
	private static Logger log = Logger.getLogger(Config.class);
	/**
     * 供Shiro插件使用。
     */
    Routes routes;
	/**
	 * 配置常量
	 */
    @Override
	public void configConstant(Constants me) {
    	//执行初始化配置
    	super.init(me,basePath);
		
		me.setEncoding("UTF-8");//设置过滤编码
		me.setDevMode(Boolean.parseBoolean(baseProp.get("devMode")));
		me.setViewType(ViewType.FREE_MARKER);//free_marker视图
		
		me.setError403View("template/common/403.html");
		me.setError404View("template/common/404.html");
		me.setError500View("template/common/500.html");
		
		me.setMaxPostSize(1024*1024*10);//文件上传大小
		me.setBaseDownloadPath(PathKit.getWebRootPath() + "/resources");//设置下载基础路径
	}
	/**
     * 配置路由
     */
    @Override
	public void configRoute(Routes me) {
		this.routes = me;
		/*web------------------------*/
		me.add("/", MainController.class);
		me.add("/user", UserController.class);//系统账号
		me.add("/power",PowerController.class);
		me.add("/role",RoleController.class);
		
		//apidoc文档
		me.add("/apidoc/project",ProjectController.class);
		me.add("/apidoc/doc",DocController.class,"/");
		me.add("/apidoc/template",TemplateController.class,"/");//文档模板控制器
		
		me.setBaseViewPath("/WEB-INF/view");
	}
    
	/**
	 * Druid连接池
	 * @return
	 */
	public static DruidPlugin createDruidPlugin() {
		return new DruidPlugin(baseProp.get("jdbcUrl").trim(), baseProp.get("username").trim(), baseProp.get("password").trim());
	}
		
	/**
	 * 配置插件
	 */
	 @Override
	public void configPlugin(Plugins me) {
		log.info("数据库参数："+baseProp.get("jdbcUrl").trim()+"=="+baseProp.get("username").trim()+"=="+baseProp.get("password").trim());
		DruidPlugin druidPlugin = createDruidPlugin();
		me.add(druidPlugin);
		 
		SqlReporter.setLog(true);//sql进log日志
		
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		arp.setShowSql(true);
		// 配置mysql方言
		arp.setDialect(new MysqlDialect());
		me.add(arp);
		// 所有配置在 MappingKit 中搞定
		_MappingKit.mapping(arp);
        // 加载配置Shiro插件
        ShiroPlugin shiro=new ShiroPlugin(routes);
        me.add(shiro);
	}
	 
	/**
	 * 配置全局拦截器
	 */
	 @Override
	public void configInterceptor(Interceptors me) {
		//添加session
		me.add(new SessionInViewInterceptor(true));
		
		//异常拦截器
		me.add(new ExceptionInterceptor(baseProp));
	}
	/**
	 * 配置处理器
	 */
	 @Override
	public void configHandler(Handlers me) {
		me.add(new RequestHandler());
	}
	 
	@Override
	public void configEngine(Engine me) {
		
	}
}