Table: api_doc
-------------+---------------+------+-----+-------------------+---------
 Field       | Type          | Null | Key | Default           | Remarks 
-------------+---------------+------+-----+-------------------+---------
 id          | VARCHAR(32)   | NO   | PRI |                   | 主键      
 project_id  | VARCHAR(32)   | NO   |     |                   |         
 name        | VARCHAR(100)  | YES  |     |                   | 名称      
 seq         | BIGINT(19)    | NO   |     | 0                 | 序号      
 state       | INT(10)       | YES  |     | 0                 | 是否可用，0表示不启用，1表示启用，2表示文档类型
 update_date | DATETIME(19)  | YES  |     |                   |         
 create_date | TIMESTAMP(19) | NO   |     | CURRENT_TIMESTAMP | 创建时间    
-------------+---------------+------+-----+-------------------+---------

Table: api_project
-------------+---------------+------+-----+-------------------+---------
 Field       | Type          | Null | Key | Default           | Remarks 
-------------+---------------+------+-----+-------------------+---------
 id          | VARCHAR(32)   | NO   | PRI |                   |         
 parent_id   | VARCHAR(32)   | YES  |     |                   | 上级id    
 name        | VARCHAR(255)  | NO   |     |                   |         
 seq         | BIGINT(19)    | YES  |     |                   |         
 state       | INT(10)       | YES  |     | 1                 |         
 update_date | DATETIME(19)  | YES  |     |                   |         
 create_date | TIMESTAMP(19) | NO   |     | CURRENT_TIMESTAMP |         
-------------+---------------+------+-----+-------------------+---------

Table: api_template
-------------+---------------+------+-----+-------------------+---------
 Field       | Type          | Null | Key | Default           | Remarks 
-------------+---------------+------+-----+-------------------+---------
 id          | VARCHAR(32)   | NO   | PRI |                   | 主键      
 name        | VARCHAR(150)  | YES  |     |                   | 模板名称    
 content     | TEXT(65535)   | YES  |     |                   | 内容      
 update_date | DATETIME(19)  | YES  |     |                   |         
 create_date | TIMESTAMP(19) | YES  |     | CURRENT_TIMESTAMP |         
-------------+---------------+------+-----+-------------------+---------

Table: api_version
--------------+---------------+------+-----+-------------------+---------
 Field        | Type          | Null | Key | Default           | Remarks 
--------------+---------------+------+-----+-------------------+---------
 id           | VARCHAR(32)   | NO   | PRI |                   |         
 doc_id       | VARCHAR(32)   | YES  |     |                   |         
 url          | VARCHAR(200)  | YES  |     |                   |         
 version      | VARCHAR(200)  | YES  |     | 1.0.0             | 版本号     
 content_code | TEXT(65535)   | YES  |     |                   | 编辑器代码   
 content_html | TEXT(65535)   | YES  |     |                   | html内容  
 update_date  | DATETIME(19)  | YES  |     |                   |         
 create_date  | TIMESTAMP(19) | YES  |     | CURRENT_TIMESTAMP |         
--------------+---------------+------+-----+-------------------+---------

