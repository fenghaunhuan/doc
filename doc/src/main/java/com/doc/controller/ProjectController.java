package com.doc.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.doc.model.ApiProject;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.wjw.utils.GUID;
import com.wjw.utils.RegExpUtil;
import com.wjwframework.jfinal.controller.BaseController;
import com.wjwframework.jfinal.vo.Response;
import com.wjwframework.jfinal.web.shiro.ShiroUtils;

public class ProjectController extends BaseController<ApiProject>{
	
	private ApiProject proDao=ApiProject.dao;
	
	/**
     * 保存目录/根目录
     */
	@Before(value={Tx.class})
    public void save(){
    	Response response=new Response();
    	String id=getPara("id");
    	String pId=getPara("pId");
    	String name=getPara("name");
    	String seq=getPara("seq");
    	Boolean isRoot =getParaToBoolean("isRoot");//是否根目录
    	if(ShiroUtils.getRoleCoding().equals("demo") && StringUtils.isNotBlank(id) ){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
    	
    	//pid检测
    	if(isRoot == null){//根目录不用判断
    		if(StringUtils.isBlank(pId)){
    			response.setErrcode(1);
    			response.setMsg("父目录不能为空!");
    			renderJson(response);
    			return;
    		}
    	}
    	
    	//名称检测
    	if(StringUtils.isBlank(name)){
    		response.setErrcode(1);
    		response.setMsg("名称不能为空!");
    		renderJson(response);
    		return;
    	}
    	
    	//排序序号必须为数字
    	if(StringUtils.isNotBlank(seq)){
			if(!RegExpUtil.isNumeric(seq)){
				response.setErrcode(1);
        		response.setMsg("排序序号不是数字!");
        		renderJson(response);
        		return;
			}
		}
    	
    	Boolean b=false;
    	ApiProject item=new ApiProject();
    	if(isRoot == null){//非根项目
    		item.setParentId(pId);
    	}
    	item.setName(name);
    	
    	if(StringUtils.isNotBlank(seq)){
    		item.setSeq(Long.parseLong(seq));
    	}else{//如果序号为空则不修改，新增则自动+1
    		if(StringUtils.isBlank(id)){//新增
    			item.setSeq(proDao.getNextSeq(pId));
        	}
    	}
    	if(StringUtils.isBlank(id)){//新增
    		item.setId(GUID.create());//创建uuid
        	b=item.save();//新增
    	}else{//更新
    		item.setId(id);
    		b=item.update();//更新
    	}
		if (!b) {
			response.setMsg("保存失败");
			response.setErrcode(3);
		}
		renderJson(response);
    }
	
	/**
	 * 获取根项目编辑模板
	 */
	public void getRootProModel(){
		setAttr("req", getRequest());
		String id=getPara("id");
		Record bean = proDao.findBeanById(id);//查询目录对象
		if(bean!=null){
			setAttr("bean", bean);
		}
		renderFreeMarker("/template/apidoc/rootPro.html");
	}
	
	/**
	 * 获取指定目录模板
	 */
	public void getProModel(){
		setAttr("req", getRequest());
		String id=getPara("id");
		Record bean = proDao.findBeanById(id);//查询目录对象
		if(bean!=null){
			setAttr("bean", bean);
		}
		renderFreeMarker("/template/apidoc/pro.html");
	}
	
	/**
	 * 编辑目录时获取该项目全部子目录
	 */
	public void getDirsByRootId(){
		Response response=new Response();
		String rootId=getPara("rootId");//获取根项目id
		Record bean = proDao.findBeanById(rootId);//查询根目录对象
		List<Record> list =  new ArrayList<Record>();
		list.add(bean);
		getListByPid(rootId, list);//递归查询子栏目集合
		response.result.put("proList",list);
		renderJson(response);
	}
	
    /**
     * 删除项目
     */
    @Before(value={Tx.class})
    public void del(){
    	Response response=new Response();
    	if(ShiroUtils.getRoleCoding().equals("demo") ){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
    	String id=getPara("id");//项目id
    	//判断该目录下是否有文档关联数据   还有子目录是否有文档
    	if(proDao.isExistDocRelate(id)){
    		response.setErrcode(8);
    		response.setMsg("该目录以及子目录下有文档！");
    		renderJson(response);
    		return;
    	}
    	
         Boolean b=proDao.deleteByIds(id);
         if(!b){
        	response.setErrcode(3);
          	response.setMsg("删除失败");
          }
         renderJson(response);
    }
    
    /**
	 * 后台首页模板   根项目集合
	 */
	public void home(){
		setAttr("req", getRequest());
		List<Record> list=proDao.findAllRootList();//查询所有根项目对象
		setAttr("list", list);
		renderFreeMarker("/WEB-INF/view/admin/home.html");
	}
	
	/**
	 * 根项目首页管理
	 */
	public void index(){
		setAttr("req", getRequest());
		String rootId = getPara("rootId");//项目id
		ApiProject bean = proDao.findById(rootId);
		setAttr("bean", bean);
		List<Record> list =  new ArrayList<Record>();
		getListByPid(rootId, list);//递归查询子栏目集合
		setAttr("beanList", list);
		renderFreeMarker("/template/apidoc/index.html");
	}
	
	private void getListByPid(String id,List<Record> list){
		List<Record> flagList = proDao.findListByParentId(id);//查询所有根项目对象
		list.addAll(flagList);
		for(Record bean : flagList){
			List<Record> itemList = proDao.findListByParentId(bean.getStr("id"));//查询所有根项目对象
			if(itemList.isEmpty()){
				bean.set("node", 1);//是节点目录
			}else{
				bean.set("node", 0);
				getListByPid(bean.getStr("id"), list);
			}
		}
	}
}
