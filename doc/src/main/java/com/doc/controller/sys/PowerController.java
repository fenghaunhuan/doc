package com.doc.controller.sys;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.wjw.utils.GUID;
import com.wjw.utils.ListUtil;
import com.wjwframework.jfinal.controller.BaseController;
import com.wjwframework.jfinal.vo.PageNavation;
import com.wjwframework.jfinal.vo.Response;
import com.wjwframework.jfinal.web.model.SysPower;
import com.wjwframework.jfinal.web.model.SysRole;
import com.wjwframework.jfinal.web.model.SysRolePower;
import com.wjwframework.jfinal.web.shiro.ShiroUtils;
import com.wjwframework.jfinal.web.verify.SysPowerVerify;


public class PowerController extends BaseController<SysPower>{
	
	private SysPower pDao=SysPower.dao;
	private SysRole rDao=SysRole.dao;
	private SysRolePower rpDao=SysRolePower.dao;
	/**
	 * 查询权限菜单集合JSON
	 */
	public void getData(){
		Response response=new Response();
		PageNavation page=getPageNavation(getRequest());
		page=pDao.findList(page);//获取集合对象
		response.result.put("totalRow", page.getTotalRow());//总记录数
		response.result.put("totalPage", page.getTotalPage());//总记录数
		response.result.put("list",page.getParamMap().get("list"));//数据集
		response.result.put("pageNumList", page.getPageNumList());//分页页码列表
		renderJson(response);
	}
    
	/**
     * 保存
     */
	@Before(value={Tx.class,SysPowerVerify.class})
    public void save(){
    	Response response=new Response();
    	SysPower power=getModel(SysPower.class,"power");
    	if(ShiroUtils.getRoleCoding().equals("demo") && StringUtils.isNotBlank(power.getId())){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
    	//空值判断
    	if(StringUtils.isBlank(power.getPowerName()) || StringUtils.isBlank(power.getPermission()) || StringUtils.isBlank(power.getUrl())){
    		response.setErrcode(1);
    		response.setMsg("名称/权限代码/url不能为空!");
    		renderJson(response);
    		return;
    	}
    	//当为操作时显示位置不能为空
    	if(power.getType().equals("O") && StringUtils.isBlank(power.getPosition())){
    		response.setErrcode(1);
    		response.setMsg("操作显示位置不能为空!");
    		renderJson(response);
    		return;
    	}
    	
    	Map<String, Object> paramMap=new HashMap<String, Object>();
    	//检测权限代码唯一性
		paramMap.put("permission",power.getPermission());
		paramMap.put("id", power.getId());
		if(pDao.isExistPermission(paramMap)){
			response.setErrcode(7);
    		response.setMsg("权限代码已存在!");
    		renderJson(response);
    		return;
		}
		
    	//如果序号为空获取下一个序号
    	if(power.getSeq()==null){
    		power.setSeq(pDao.getNextSeq());
    	}
    	String[] position=getParaValues("power.position");
    	if(position!=null){
    		power.set("position",Arrays.asList(position).toString());
    	}
    	 Boolean b=false;
    	if(power.getId()!=null){
    		b=power.update();//更新
    	}else{
    		power.setId(GUID.create());//生成主键id
    		b=power.save();//添加
    	}
        if(!b){
        	response.setErrcode(3);
        	response.setMsg("保存失败");
        }
        renderJson(response);
    }
	
    /**
     * 复制
     */
    public void copy() {
		save();
    }
    
    /**
     * 删除
     */
    @Before(value={Tx.class})
    public void del(){
		Response response = new Response();
		if(ShiroUtils.getRoleCoding().equals("demo") ){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
		String ids=ListUtil.getStrSqlByStrs(getPara("ids"));
		
    	//判断该角色是否有用户关联
    	if(pDao.isExistPowerRelate(ids)){
    		response.setErrcode(8);
    		response.setMsg("菜单下面有子菜单关联信息!");
    		renderJson(response);
    		return;
    	}
    	
		Boolean b = pDao.deleteByIds(ids);
		if (!b) {
			response.setErrcode(3);
			response.setMsg("删除失败");
		}
		renderJson(response);
    }
    /**
     * 根据id获取power对象
     */
    public void findById(){
    	Response response=new Response();
    	String id=getPara("id");
    	if(id==null){
    		response.setErrcode(1);
    		response.setMsg("id不能为空!");
    		renderJson(response);
    		return;
    	}
    	SysPower power=pDao.findById(id);
		response.result.put("power",power);//查询菜单对象
    	renderJson(response);
    }
    
    /**
     * 授权时zTree一次获取全部数据并将拥有的权限选中
     */
    public void getTreeAllData(){
    	Response response=new Response();
    	String id=getPara("id");
    	//空值检测
    	if(id==null){
    		response.setErrcode(1);//参数为空
    		response.setMsg("角色id不能为空");
    		renderJson(response);
    		return;
    	}
    	SysRole r=rDao.findById(id);//查询当前角色
    	List<Record> authList=rpDao.findPowersByRoleId(r.getId(),r.getCoding());//获取角色所拥有的权限集合
    	List<Record> allList=pDao.getAllList();//获取全部权限集合
    	List<Record> list=new ArrayList<Record>();//封装集合
    	//拼装Tree数据
		for(Record re:allList){
			re.set("text",re.get("power_name"));
			Boolean flg=false;
			for(Record item:authList){
				if(item.getStr("id").equals(re.getStr("id"))){
					re.set("checked", true);
					flg=true;
					break;
				}
			}
			if(!flg){
				re.set("checked", false);
			}
			re.set("isParent ",re.get("type").equals("M")?true:false);//是否父节点
			re.set("open",false);//打开
			list.add(re);
		}
    	response.result.put("list",list);//全部权限集合
    	renderJson(response);
    }
    
	/**
	 * 获取全部菜单
	 */
	public void getAllData(){
		Response response=new Response();
		List<Record> list=pDao.getAllList();//获取全部权限集合
		response.result.put("list",list);
		renderJson(response);
	}
}
