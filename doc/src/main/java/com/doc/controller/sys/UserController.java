package com.doc.controller.sys;


import java.util.HashMap;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.wjw.utils.GUID;
import com.wjw.utils.ListUtil;
import com.wjwframework.jfinal.controller.BaseController;
import com.wjwframework.jfinal.vo.PageNavation;
import com.wjwframework.jfinal.vo.Response;
import com.wjwframework.jfinal.web.constant.Constants;
import com.wjwframework.jfinal.web.model.SysUser;
import com.wjwframework.jfinal.web.shiro.ShiroUtils;
import com.wjwframework.jfinal.web.verify.SysUserVerify;


public class UserController extends BaseController<SysUser>{
	
	private SysUser uDao=SysUser.dao;
	/**
	 * 查询用户集合JSON
	 */
	public void getData(){
		Response response=new Response();
		PageNavation page=getPageNavation(getRequest());
		page=uDao.findList(page);//获取集合对象
		response.result.put("totalRow", page.getTotalRow());//总记录数
		response.result.put("totalPage", page.getTotalPage());//总记录数
		response.result.put("list",page.getParamMap().get("list"));//数据集
		response.result.put("pageNumList", page.getPageNumList());//分页页码列表
		renderJson(response);
	}
    /**
     * 添加/修改 用户 	
     */
	@Before(value={Tx.class,SysUserVerify.class})
    public void save(){
    	Response response=new Response();
    	if(ShiroUtils.getRoleCoding().equals("demo")){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
    	SysUser user=getModel(SysUser.class,"user");
    	Boolean isAdd=user.getId()==null;
    	
    	//空值判断
    	if(user.getUserName()==null || user.getPhone()==null ||user.getMail()==null||user.getRoleId()==null){
    		response.setErrcode(1);
    		response.setMsg("用户名、手机号、邮箱或角色不能为空!");
    		renderJson(response);
    		return;
    	}
    	//添加时
    	if( isAdd && user.getPassword()==null){//添加
    		response.setErrcode(1);
    		response.setMsg("密码不能为空!");
    		renderJson(response);
    		return;
    	}
    	//检测用户名是否存在
    	Map<String, Object> paramMap=new HashMap<String, Object>();
    	paramMap.put("user_name",user.getUserName());
    	if( !isAdd ){
    		paramMap.put("id", user.getId());
    	}
    	if(uDao.isExistUserName(paramMap)){
    		response.setErrcode(3);
    		response.setMsg("用户名已存在!");
    		renderJson(response);
    		return;
    	}
    	//检测手机号是否存在
    	paramMap.put("phone",user.getPhone());
    	if(uDao.isExistPhone(paramMap)){
    		response.setErrcode(4);
    		response.setMsg("手机号已存在!");
    		renderJson(response);
    		return;
    	}
    	//检测邮箱是否存在
    	paramMap.put("mail",user.getMail());
    	if(uDao.isExistMail(paramMap)){
    		response.setErrcode(5);
    		response.setMsg("邮箱已存在!");
    		renderJson(response);
    		return;
    	}
    	Boolean flag=false;
    	if( isAdd ){//添加
    		user.setId(GUID.create());//生成id
    		if(user.getImage()==null){
    			user.setImage("/images/system/web_user.png");//默认用户头像
    		}
    		//如果序号为空则自动增长
    		if(user.getSeq()==null){
    			user.setSeq(user.getNextSeq());
    		}
    		flag=user.save();
    	}else{//修改
    		//如果序号为空则不修改
        	if(user.getSeq()==null){
        		user.remove("seq");
        	}
        	flag=user.update();
    	}
        if(!flag){
        	response.setErrcode(3);
        	response.setMsg("保存失败");
        }
       renderJson(response);
    }
    /**
     * 删除用户
     */
	@Before(value={Tx.class})
    public void del(){
		Response response=new Response();
		if(ShiroUtils.getRoleCoding().equals("demo")){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
		String ids=ListUtil.getStrSqlByStrs(getPara("ids"));
		
		Boolean b=uDao.deleteByIds(ids);
		if(!b){
			response.setErrcode(3);
			response.setMsg("删除失败");
		}
		renderJson(response);
    }
    /**
     * 根据id查询用户信息
     */
    public void findById(){
    	Response response=new Response();
    	String id=getPara("id");
    	if(id==null){//查询用户信息应剔除密码等铭感字段
    		response.setErrcode(1);
    		response.setMsg("id不能为空!");
    		return;
    	}
		response.result.put("user",uDao.findById(id));//查询用户对象
    	renderJson(response);
    }
    
    /**
     * 修改密码
     */
    @Before(value={Tx.class})
    public void updPwd(){
    	Response response=new Response();
    	if(ShiroUtils.getRoleCoding().equals("demo")){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
    	String oldPwd=getPara("oldPwd");//原密码
    	String newPwd=getPara("newPwd");//新密码
    	String newPwd2=getPara("newPwd2");//确认密码
    	SysUser user=ShiroUtils.getSysUser();
    	//空值判断
    	if(oldPwd.equals("")||oldPwd==null||newPwd2.equals("") || newPwd2 == null||newPwd.equals("") || newPwd == null){
    		response.setErrcode(1);
    		response.setMsg("原密码、新密码或确认密码不能为空");
    		renderJson(response);
    		return;
    	}
    	//原密码验证
    	if(!user.getPassword().equals(oldPwd)){
    		response.setErrcode(1);
    		response.setMsg("原密码不正确");
    		renderJson(response);
    		return;
    	}
    	//确认密码验证
    	if(!newPwd2.equals(newPwd)){
    		response.setErrcode(1);
    		response.setMsg("确认密码不一致");
    		renderJson(response);
    		return;
    	}
    	//更新密码，并更新session
    	user.set("password", newPwd);
    	user.update();//跟新用户
    	setSessionAttr(Constants.SESSION_USER_KEY, user);//跟新session
		renderJson(response);
    }
}

