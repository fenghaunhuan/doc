package com.doc.controller.sys;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.wjw.utils.ListUtil;
import com.wjw.utils.RegExpUtil;
import com.wjwframework.jfinal.controller.BaseController;
import com.wjwframework.jfinal.vo.PageNavation;
import com.wjwframework.jfinal.vo.Response;
import com.wjwframework.jfinal.web.model.SysRole;
import com.wjwframework.jfinal.web.model.SysRolePower;
import com.wjwframework.jfinal.web.shiro.ShiroUtils;
import com.wjwframework.jfinal.web.verify.SysRoleVerify;

public class RoleController extends BaseController<SysRole>{
	private Logger log=LoggerFactory.getLogger(getClass());
	private SysRole rDao=SysRole.dao;
	private SysRolePower rpDao=SysRolePower.dao;
	
	/**
	 * 查询权限菜单集合JSON
	 */
	public void getData(){
		Response response=new Response();
		PageNavation page=getPageNavation(getRequest());
		page=rDao.findList(page);//获取集合对象
		response.result.put("totalRow", page.getTotalRow());//总记录数
		response.result.put("totalPage", page.getTotalPage());//总记录数
		response.result.put("list",page.getParamMap().get("list"));//数据集
		response.result.put("pageNumList", page.getPageNumList());//分页页码列表
		renderJson(response);
	}
    /**
     * 保存/批量保存
     */
	@Before(value={Tx.class})
    public void save(){
    	Response response=new Response();
    	//获取邀请成员userSON字符串并转换为fastJSONObject
    	JSONArray json=null;
    	try {
    		json= JSONObject.parseArray(getPara("roles"));
		} catch (JSONException e) {
			response.setErrcode(14);
			response.setMsg("roles的JSON格式不正确");
			renderJson(response);
			return;
		}
    	List<Record> reList=rDao.parseJson(json);//解析出的所有角色
    	List<String> codList=new ArrayList<String>();//编码集合
    	List<Integer> addRoleIndex=new ArrayList<Integer>();//符合条件的行序号
    	if(reList.size()<1){
    		response.setErrcode(1);
    		response.setMsg("seq不是数字!");
    		renderJson(response);
    		return;
    	}
    	//批量检测空值
    	for(Record re:reList){
    		//空值检测
        	if(re.getStr("coding")==null||re.getStr("role_name")==null){
        		response.setErrcode(1);
        		response.setMsg("编码、名称、codign不能为空!");
        		renderJson(response);
        		return;
        	}
        	//存储编码
        	codList.add(re.getStr("coding"));
        	//存储行序号
        	addRoleIndex.add(re.getInt("index"));
        	re.remove("index");//清除index
    	}
    	//编码重复判断
    	for(String item:codList){
    		Integer indexOf=codList.indexOf(item);//第一次出现的索引
    		Integer lastIndexOf=codList.lastIndexOf(item);//最后一次出现的索引
    		if(indexOf!=lastIndexOf){
    			response.setErrcode(7);
        		response.setMsg(item+"角色编码重复!");
        		renderJson(response);
        		return;
    		}
    	}
    	//编码唯一判断
    	List<Record> codRecord=rDao.isExistCodings(ListUtil.getStrByList(codList));
    	if(codRecord.size()>0){
    		response.setErrcode(7);
    		String msg="[";
    		for(Record re:codRecord){
    			msg+="'"+re.getStr("coding")+"',";
    		}
    		msg+="]角色编码系统已存在!";
    		response.setMsg(msg);
    		renderJson(response);
    		return;
    	}
    	Db.batchSave("sys_role", reList, 500);//批量保存
    	response.result.put("addRoleIndex",addRoleIndex);
        renderJson(response);
    }

    /**
     * 修改
     */
	@Before(value={Tx.class,SysRoleVerify.class})
    public void editor() {
    	Response response=new Response();
    	if(ShiroUtils.getRoleCoding().equals("demo")){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
    	String id=getPara("id");
    	String role_name=getPara("role_name");
    	String coding=getPara("coding");
    	String seq=getPara("seq");
    	
    	//空值检测
    	if(StringUtils.isBlank(coding) || StringUtils.isBlank(role_name)){
    		response.setErrcode(1);
    		response.setMsg("编码、名称不能为空!");
    		renderJson(response);
    		return;
    	}
    	//排序序号是否数字
    	if(StringUtils.isNotBlank(seq)){
			if(!RegExpUtil.isNumeric(seq)){
				response.setErrcode(1);
        		response.setMsg("排序序号不是数字!");
        		renderJson(response);
        		return;
			}
		}
		//检测coding唯一性
    	Map<String, Object> paramMap=new HashMap<String, Object>();
    	paramMap.put("coding",coding);
    	paramMap.put("id",id);
    	if(rDao.isExistCoding(paramMap)){
    		response.setErrcode(7);
    		response.setMsg("角色编码已存在!");
    		renderJson(response);
    		return;
    	}
    	SysRole item=new SysRole();
    	item.setId(id);
    	item.setRoleName(role_name);
    	item.setCoding(coding);
    	//如果序号为空则不修改
    	if(StringUtils.isNotBlank(seq)){
    		item.setSeq(Long.parseLong(seq));
    	}
        Boolean b=item.update();
        if(!b){
         	response.setMsg("修改失败");
         	response.setErrcode(3);
         }
        renderJson(response);
    }
    /**
     * 根据id获取角色对象
     */
    public void findById(){
    	Response response=new Response();
    	String id=getPara("id");
    	if(id==null){
    		response.setErrcode(1);
    		response.setMsg("id主键不能为空!");
    		return;
    	}
		response.result.put("role",rDao.findById(getPara("id")));//查询角色对象
    	renderJson(response);
    }
    /**
     * 获取所有角色集合
     */
    public void getAllList(){
    	Response response=new Response();
		List<Record> reList=rDao.getCommDbUtil().findAllSort("");
		response.result.put("list",reList);//查询角色对象
    	renderJson(response);
    }
    /**
     * 删除角色
     */
    @Before(value={Tx.class})
    public void del(){
    	Response response=new Response();
    	if(ShiroUtils.getRoleCoding().equals("demo")){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
    	String ids=ListUtil.getStrSqlByStrs(getPara("ids"));
    	
    	//判断该角色是否有用户关联
    	if(rDao.isExistUserRelate(ids)){
    		response.setErrcode(8);
    		response.setMsg("角色下面有用户关联信息!");
    		renderJson(response);
    		return;
    	}
    	
         Boolean b=rDao.deleteByIds(ids);
         if(!b){
        	response.setErrcode(3);
          	response.setMsg("删除失败");
          }
         renderJson(response);
    }
    /**
     * 保存授权信息
     */
    @Before(value={Tx.class})
    public void saveAuth(){
    	Response response=new Response();
    	if(ShiroUtils.getRoleCoding().equals("demo")){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
    	String ids=getPara("ids");//获取选中的权限
    	String role_id=getPara("role_id");//获取用户角色id
    	if(ids==null || role_id==null){
    		response.setErrcode(1);
    		response.setMsg("角色id、所选权限ids!");
    		renderJson(response);
    		return;
    	}
    	List<String> updIds=Arrays.asList(ids.split(","));//修改后
    	List<String> oldList=new ArrayList<String>();//原来的权限集合
    	SysRole r=rDao.findById(role_id);//查询当前角色
    	List<Record> authList=rpDao.findPowersByRoleId(r.getId(),r.getCoding());//获取角色所拥有的权限集合
    	for(Record  re:authList){
    		oldList.add(re.getStr("id"));
    	}
    	List<String> delList=ListUtil.substractList(oldList, updIds);//删除的role_power
    	List<String> addList=ListUtil.substractList(updIds, oldList);//新增role_power
    	
    	if(delList.size()>0){//删除权限
    		rpDao.deleteByPowerAndRole(ListUtil.getStrByList(delList),role_id);
    	}
    	if(addList.size()>0){//添加权限
    		List<SysRolePower> rpList=new ArrayList<SysRolePower>();
    		for(String id:addList){
    			SysRolePower rp=new SysRolePower();
    			rp.setPowerId(id);//权限id
    			rp.setRoleId(role_id);//角色代码
    			rpList.add(rp);
    		}
    		int[] count=Db.batchSave(rpList,1000);//添加权限数据
    		for(int i:count){
    			if(i<=0){
    				String msg="[角色管理]错误信息：授权信息["+addList.get(i)+"]添加失败!";
    				log.error(msg);
    				response.setErrcode(3);
    				response.setMsg(response.getMsg()+"\\n"+msg);
    			}
    		}
    	}
    	renderJson(response);
    }
}
