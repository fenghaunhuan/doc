package com.doc.controller;

import java.util.List;

import com.doc.model.ApiTemplate;
import com.wjw.utils.GUID;
import com.wjwframework.jfinal.controller.BaseController;
import com.wjwframework.jfinal.vo.Response;
import com.wjwframework.jfinal.web.shiro.ShiroUtils;

public class TemplateController extends BaseController<ApiTemplate>{
	
	
	private ApiTemplate teplateDao =ApiTemplate.dao;
	
	
	/**
	 * 获取模板列表页面
	 */
	public void getList(){
		setAttr("req", getRequest());
		List<ApiTemplate>  beanList = teplateDao.find("SELECT id,name,create_date FROM api_template order by create_date asc");
		setAttr("beanList", beanList);
		renderFreeMarker("/template/apidoc/template/index.html");
	}
	
	/**
	 * 获取指定模板
	 */
	public void getTemplate(){
		setAttr("req", getRequest());
		String id = getPara("id");
		ApiTemplate bean = teplateDao.findById(id);
		setAttr("bean", bean);
		renderJson(bean);
	}
	
	/**
	 * 保存模板
	 */
	public void save(){
		Response resp =new Response();
		String name = getPara("name");
		String content = getPara("content");
		ApiTemplate bean = new ApiTemplate();
		bean.setId(GUID.create());
		bean.setName(name);
		bean.setContent(content);
		bean.save();
		renderJson(resp);
	}
	
	/**
	 * 删除指定模板
	 */
	public void del(){
		Response response = new Response();
		if(ShiroUtils.getRoleCoding().equals("demo") ){
    		response.setErrcode(-1);
    		response.setMsg("演示demo,无权限操作!");
    		renderJson(response);
    		return;
    	}
		setAttr("req", getRequest());
		String id = getPara("id");
		Boolean flag = teplateDao.deleteById(id);
		if(!flag){
			response.setErrcode(-1);
			response.setMsg("服务器异常，稍后请求！");
		}
		renderJson(response);
	}
}
