package com.doc.plugin;

import javax.sql.DataSource;

import com.doc.config.Config;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.generator.BaseModelGenerator;
import com.jfinal.plugin.activerecord.generator.DataDictionaryGenerator;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.activerecord.generator.MappingKitGenerator;
import com.jfinal.plugin.activerecord.generator.ModelGenerator;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.wjwframework.jfinal.config.BaseConfig;

/**
 * @className MyGenerator
 * @description 基础模型代码生产工具
 * @author wjw
 * @cTime 2016年3月19日下午5:49:47
 */
public class MyGeneratorSys  extends  Generator{
	
	public static DataSource getDataSource() {
		BaseConfig.baseProp=PropKit.use(Config.basePath);//加载数据库配置文件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(BaseConfig.baseProp.get("jdbcUrl"), BaseConfig.baseProp.get("username"), BaseConfig.baseProp.get("password").trim());
		c3p0Plugin.start();
		return c3p0Plugin.getDataSource();
	}

	public MyGeneratorSys (DataSource dataSource, String baseModelPackageName, String baseModelOutputDir, 
			String modelPackageName, String modelOutputDir,String mappingKitPack,String mappingKitOutputDir){
        super(dataSource, new BaseModelGenerator(baseModelPackageName, baseModelOutputDir), 
        		new ModelGenerator(modelPackageName, baseModelPackageName, modelOutputDir));
        mappingKitGenerator = new MappingKitGenerator(mappingKitPack,mappingKitOutputDir);
        dataDictionaryGenerator = new DataDictionaryGenerator(dataSource,mappingKitOutputDir);
    }
	
	public static void main(String[] args) {
		// base model 所使用的包名
		String baseModelPackageName = "com.manage.apidoc.model.base";
		// base model 文件保存路径
		String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/manage/apidoc/model/base";
		
		// model 所使用的包名 
		String modelPackageName = "com.manage.apidoc.model";
		// model 文件保存路径 
		String modelOutputDir = baseModelOutputDir + "/..";
		
		// MappingKit 使用的包名
		String mappingKitPack = "com.manage.common.config";
		// MappingKit 与 DataDictionary 文件默认保存路径
		String mappingKitOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/manage/common/config";
		
		// 创建生成器
		MyGeneratorSys gernerator = new MyGeneratorSys(getDataSource(), baseModelPackageName, baseModelOutputDir,modelPackageName,
				modelOutputDir,mappingKitPack,mappingKitOutputDir);
		// 添加不需要生成的表名
//		String[] excTab={"api_doc","api_project","api_version"};
		String[] excTab={"sys_power","sys_role","sys_role_power","sys_user"};
		
		gernerator.addExcludedTable(excTab);
		
		// 设置是否在 Model 中生成 dao 对象
		gernerator.setGenerateDaoInModel(true);
		// 设置是否生成字典文件
		gernerator.setGenerateDataDictionary(true);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
		gernerator.setRemovedTableNamePrefixes("");
		// 生成
		gernerator.generate();
		System.out.println("==="+baseModelOutputDir);
	}
}
