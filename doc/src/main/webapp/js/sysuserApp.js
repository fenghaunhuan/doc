// 系统账户管理
remanApp.controller("userCtrl", function($scope, $http, $modal,$compile,$state){
	$scope.pageSize = 15;//默认初始值
	$scope.totalPage = 0;//总页码
	$scope.pageNow = 1;//当前页码
	$scope.totalRow = 0;//总数量
	$scope.sort="";//排序字段
	$scope.order="";//排序方式
	$scope.search="";//搜索字段
	$scope.pageNumList = [];//分页页码
	$scope.pageList=[10,15,20,50,100,200];//设置分页数的列表
	
	$scope.WEBROOT=WEBROOT;
	$scope.useBatchStatus=false;//默认隐藏批处理按钮
	$scope.userList = [];//用户数据集合
	
	$scope.tab = 1;//默认选项卡
	$scope.isSelected = function(setTab){
		return $scope.tab === setTab;
	};
	
	//表格数据源初始化
	$scope.init=function(){
		$scope.totalPage = 0;//总页码
		$scope.pageNow = 1;//当前页码
		$scope.totalRow = 0;//总数量
		$scope.useBatchStatus=false;//默认隐藏批处理按钮
		$scope.search=$("#search").parent().prev().val();//重新检搜索字段
		$scope.loadList($scope.pageNow, $scope.pageSize,$scope.sort,$scope.order,$scope.search);
	}
	
	// 查询全部记录数据
	$scope.loadList = function(pageNow, pageSize,sort,order,search){
		var layerId = layer.load(1);
		var params={"pageNow":pageNow,"pageSize":pageSize,"sort":sort,"order":order,"search":search};
		$http.post(WEBROOT + "/user/getData.do",params)
		.success(function(data, status){
			if(data.errcode!=0){
				layer.alert(data.msg,{icon:0});
			} else {
				$scope.totalRow = data.result.totalRow;
				$scope.totalPage = data.result.totalPage;
				$scope.userList = data.result.list;
				$scope.pageNumList=data.result.pageNumList;
			}
			layer.close(layerId);
		}).error(function(data, status){});
	};
	//排序
	$scope.sortFun=function(event){
		var clas=$(event.target).attr("class");//获取当前点击的样式
		$scope.sort=$(event.target).attr("data-sort");//排序字段
		$scope.order="asc";//排序方式
		$(event.target).parent().parent().parent().find("th>div>a").attr("class","header");
		if(clas==="header"){
			$(event.target).attr("class","headerSortDown");
			$scope.order="asc";
		}else if(clas==="headerSortDown"){
			$(event.target).attr("class","headerSortUp");
			$scope.order="desc";
		}else{
			$(event.target).attr("class","headerSortDown");
			$scope.order="asc";
		}
		//重新加载数据
		$scope.init();
	};
	// 改变页数
	$scope.changePage = function(page, offset){
		page = page + offset;
		if($scope.pageNow != page){
			$scope.pageNow = page;
			$scope.loadList(page, $scope.pageSize,$scope.sort,$scope.order,$scope.search);
		}
	};
	//改变每页显示量
	$scope.pageSizeSelectChange = function(pageSize){
		//还原为默认值
		$scope.pageSize = pageSize;
		$scope.init();
	};
	// 判断是否第1页
	$scope.isFirst = function(){
		return $scope.pageNow == 1;
	};
	// 判断是否未页
	$scope.isLast = function(){
		return $scope.totalPage == $scope.pageNow;
	};
	// 判断是否当前页
	$scope.isCurrent = function(page){
		return $scope.pageNow == page;
	};
	//全选
	$scope.checkAll = function(event){
		if(event!=null){
			$('#data-list>tbody>tr>td>input[type="checkbox"]').prop("checked", $(event.target).is(":checked"));
			if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
				$scope.useBatchStatus = true;
			} else {
				$scope.useBatchStatus = false;
			}
		}else{
			$('#data-list>tbody>tr>td>input[type="checkbox"]').prop("checked",true);
			
			if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
				$scope.useBatchStatus = true;
			} else {
				$scope.useBatchStatus = false;
			}
		}
	};
	//反选
	$scope.reverse = function(){
		//获取当前行复选框是否选中再取反向选择
		$('#data-list>tbody>tr>td>input[type="checkbox"]').each(function(i){
			$(this).prop("checked",!$(this).prop("checked"))
		});
		
		if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
			$scope.useBatchStatus = true;
		} else {
			$scope.useBatchStatus = false;
		}
	};
	// 检测是否显示隐藏按钮
	$scope.checkSelect = function(){
		if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
			$scope.useBatchStatus = true;
		} else {
			$scope.useBatchStatus = false;
		}
	};
	//增强点击行选中
	$scope.hitRow = function(event){
		var obj=event.target;
		var item=$(obj).parent().children("td").first().children("input[type='checkbox']");
		item.prop("checked",!item.prop("checked"));
		//检测是否显示隐藏按钮
		$scope.checkSelect();
	};
	//删除
	$scope.del = function(event){
		var ids = [];
		if(event!=null){
			var id=$(event.target).parent().parent().attr("id");
			ids.push(id);
		}else{
			$('#data-list tbody>tr>td>input[type="checkbox"]:checked').each(function(index){
				ids.push($(this).parent().parent().attr("id"));
			});
		}
		//删除
		if(ids.length != 0){
			layer.confirm('确认是否删除？', {
			    btn: ['确认','取消'] //按钮
			}, function(){
				var layerId = layer.load(1);
				$http.post(WEBROOT + "/user/del.do", {"ids":ids.join(",")})
				.success(function(data, status){
					if(data.errcode!=0){
						layer.alert(data.msg,{icon:0});
					} else {
						layer.msg('删除成功', {icon: 1});
						$scope.init();
					}
					layer.close(layerId);
				}).error(function(data, status){});
			}, function(){
			    
			});
		}
	};

	//显示头像
	$scope.showImg=function(event){
		layer.open({
			  type: 1,
			  shade: false,
			  title: false, //不显示标题
			  content:"<img width='100px' src='"+$(event.target).attr("src")+"'>", 
			  cancel: function(index){
			  }
		});
	}
});

remanApp.controller("userAddCtrl", function($scope, $http, $modal,$compile,$state){
	$scope.roleList=[];
	$scope.showTree=false;
	$scope.role_id="";
	$scope.role_name="";
	
	// 初始化树
	$scope.init = function(){
		$.post(WEBROOT+"/role/getAllList.do",function(data){
			if(data.errcode!=0){
				$("#formError").text(data.msg);
				$("#formError").show();
			}else{
				$scope.roleList=data.result.list;
				//初始化树
				$('#role_tree').tree({
					animate:true,
					initialState:"normal"
				});
				//点击树事件
				$scope.selectRole = function(event){
					$scope.role_name=$(event.target).html();
					$scope.role_id=$(event.target).attr("attr-id");
					$("#role_id").val($scope.role_name);
					$scope.showTree=false;
				};
			};
		},"json");
	};
   //显示树
   $scope.show_tree = function(){
	   $scope.showTree=!$scope.showTree;
   };
   
	$scope.saveBtn = function(){
		var layerId = layer.load(1);
		$http.post(WEBROOT + "/user/save.do",$("#form").serialize()+"&user.role_id="+$scope.role_id+"&user.password="+hexMD5($("#password").val()))
		.success(function(data, status){
			layer.close(layerId);
			if(data.errcode!=0){
				$("#formError").text(data.msg);
				$("#formError").show();
			} else {
				layer.msg("新增成功",{icon:1});
				$("#formError").hide();
				document.getElementById("form").reset(); 
			}
		}).error(function(data, status){});				
	};
});

remanApp.controller("userEditorCtrl", function($scope, $http,$stateParams,$modal,$state){
	if($stateParams.id){
		var id=$stateParams.id;
		var layerId = layer.load(1);
		$http.post(WEBROOT + "/user/findById.do", "id="+id)
		.success(function(data, status){
			layer.close(layerId);
			if(data.errcode!=0){
				layer.alert(data.msg,{icon:0});
			} else {
				$scope.id=data.result.user.id;
				$scope.mail=data.result.user.mail;
				$scope.user_name=data.result.user.user_name;
				$scope.phone=data.result.user.phone;
				$scope.seq=data.result.user.seq;
				$scope.state=data.result.user.state;
				$scope.sex=data.result.user.sex;
				$scope.role_id=data.result.user.role_id;
				$scope.role_name=data.result.user.role_name;
				//设置性别
				if($scope.sex==1){
					$("input[name='user.sex'][value='1']").attr("checked",true);
					  $scope.typeState=true;
				}else{
					$("input[name='user.sex'][value='2']").attr("checked",true);
					 $scope.typeState=false;
				}
				//设置状态
				if($scope.state==1){
					$("input[name='user.state'][value='1']").attr("checked",true);
					  $scope.typeState=true;
				}else{
					$("input[name='user.state'][value='2']").attr("checked",true);
					 $scope.typeState=false;
				}
			}
		}).error(function(data, status){});
		
		// 初始化树
		$scope.init = function(){
			$http.post(WEBROOT + "/role/getAllList.do")
			.success(function(data, status){
				if(data.errcode!=0){
					$("#formError").text(data.msg);
					$("#formError").show();
				} else {
					$scope.roleList=data.result.list;
					//初始化树
					$('#role_tree').tree({
						animate:true,
						initialState:"normal"
					});
					//点击树事件
					$scope.selectRole = function(event){
						$scope.role_name=$(event.target).html();
						$scope.role_id=$(event.target).attr("attr-id");
						$("#role_id").val($scope.role_name);
						$scope.showTree=false;
					};
				}
			}).error(function(data, status){});
		};
	   //显示树
	   $scope.show_tree = function(){
		   $scope.showTree=!$scope.showTree;
	   };
		   
		//保存编辑按钮
		$scope.editorBtn = function(){
			var layerId = layer.load(1);
			//参数值
			var param="&user.role_id="+$scope.role_id;
			//密码处理
			var psd=$("#password").val();
			if(!isBlank(psd)){
				param=param+"&user.password="+hexMD5(psd);
			}
			$http.post(WEBROOT + "/user/save.do", $("#form").serialize()+param)
			.success(function(data, status){
				if(data.errcode!=0){
					$("#formError").text(data.msg);
					$("#formError").show();
				} else {
					layer.msg("修改成功",{icon:1});
					$state.go('user');
				}
				layer.close(layerId);
			}).error(function(data, status){});
		};
	};	
});