//侧面一级菜单
remanApp.controller("navberCtrl", function($scope, $http, $modal,$state){
	$scope.treeRoot="home";	//默认第一级菜单 active
	$scope.treeChild="home_index";    //默认第二级菜单 active
	$scope.module = "home";			//url地址
	
	$scope.isSelectRoot = function(perm){
		return $scope.treeRoot == perm;
	};
	$scope.isSelectChild = function(perm){
		return $scope.treeChild == perm;
	};
	
	//一级菜单选择事件
	$scope.selectRoot = function(perm, module){
		$scope.treeRoot = perm;
		if(module){
			$scope.module = module;
		}
	};
	//二级菜单选择事件
	$scope.selectChild = function(perm,fatherPerm,module){
		$scope.treeChild = perm;
		$scope.treeRoot = fatherPerm;
		if(module){
			$scope.module = module;
		}
	};
	// 退出系统
	$scope.logout = function(){
		layer.confirm('确认退出系统？', {
		    btn: ['退出','取消'] //按钮
		}, function(){
			$http.post(WEBROOT + "/exit.do","")
			.success(function(data){
				if(data.errcode==0){
					window.location= WEBROOT + "/admin.do";
				}else{
					layer.msg("服务器异常"+data.msg,{icon:0});
				}
			});
		}, function(){
		    
		});
	};
	//修改密码
	$scope.modPassword = function(){
		$modal.open({
			backdrop: "static",
			animation: $scope.animationsEnabled,
			templateUrl: WEBROOT + "/template/sysuser/modPassword.html",
			controller: "modpCtrl",
		});
	};
});

//修改密码控制器
remanApp.controller("modpCtrl", function($scope, $http,$stateParams,$modalInstance){
	// 修改密码
	$scope.submit = function(){
		$scope.showName = $stateParams.showName;
		if(checkNull($("#oldPwd"),$("#responser"),"原密码", true) && checkNull($("#newPwd"),$("#responser"),"新密码", true) && checkNull($("#newPwd2"),$("#responser"),"确定密码", true)){
			var layerId = layer.load(1);
			$http.post(WEBROOT + "/user/updPwd.do", {"oldPwd":hexMD5($("#oldPwd").val()),"newPwd":hexMD5($("#newPwd").val()),"newPwd2":hexMD5($("#newPwd2").val())})
			.success(function(data, status){
				layer.close(layerId);
				if(data.errcode == 0){
					$scope.cancel();
					layer.msg("密码修改成功",{icon:1});
					$(".modpassword input[type='password']").val("");
				} else {
					$("#responser").html(data.msg);
				}
			}).error(function(data, status){
				layer.close(layerId);
			});
		}
	};
	
	$scope.cancel = function(){
		$modalInstance.dismiss('cancel');
	};
});

//主页模板控制器
remanApp.controller("homeCtrl", function($scope, $http, $modal,$compile,$state,$stateParams){
	
	//创建项目、编辑项目
	$scope.createRootPro = function(event){
		var obj =$(event.target);//获取当前点击对象
		var edit_pro_id =$(obj).data("id");
		$scope.delShow=true;
		
		if(typeof(edit_pro_id)=='undefined'){
			$scope.delShow=false;
		}
		
		//删除根项目
		$scope.delPro = function(){
			if(typeof(edit_pro_id)!='undefined'){
				var layerId = layer.load(1);
				$.post(WEBROOT+"/apidoc/project/del.do", "id="+edit_pro_id,function(data){
					if(data.errcode!=0){
						$("#formError").html(data.msg);
						$('#formError').show();
					} else {
						$('#createProjectModal').modal('hide');//关闭模态框
						layer.msg('删除成功', {icon: 1});
						//当模态框被隐藏后执行事件
						$('#createProjectModal').on('hidden.zui.modal', function() {
							//跳转路由
							$state.go('home',{date:new Date()});
						});
					}
					layer.close(layerId);
				});
			}
		};
		
		$("#createProjectModal .modal-body").load(WEBROOT+"/apidoc/project/getRootProModel.do?id="+edit_pro_id, "", function(){
			//将加载的html内容再次经过angular编译
			$scope.modelHtml=$compile($("#createProjectModal .modal-body").html())($scope);//动态编译
			$("#createProjectModal .modal-body").html($scope.modelHtml);
		});
	};
	
	//保存目录
	$scope.saveProject = function(){
		//保存目录
		var layerId = layer.load(1);
		$.post(WEBROOT+"/apidoc/project/save.do", $("#editProject").serialize()+"&isRoot=true",function(data){
			if(data.errcode!=0){
				$("#formError").html(data.msg);
				$('#formError').show();
			} else {
				$('#createProjectModal').modal('hide');//关闭模态框
				layer.msg('保存成功', {icon: 1});
				//当模态框被隐藏后执行事件
				$('#createProjectModal').on('hidden.zui.modal', function() {
					//跳转路由
					$state.go('home',{date:new Date()});
				});
			}
			layer.close(layerId);
		});
	};
	
});
