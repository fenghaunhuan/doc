/** 
 * 判断是否为对象
 * 
 * obj 待判断对象
 *  */
function isObject(obj){
	if(typeof(obj) == "object"){
		return true;
	} else {
		return false;
	}
}
/**
 * 判断输入手机号
 * @param mobile
 */
function validatemobile(mobile) { 
    if(mobile.length==0) { 
       layer.alert("请输入手机号码！",{icon:0});
       return false; 
    }     
    if(mobile.length!=11) { 
        layer.alert("请输入有效的手机号码！",{icon:0});
        return false; 
    } 
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
    if(!myreg.test(mobile)) { 
    	layer.alert("请输入有效的手机号码！",{icon:0});
        return false; 
    }
    return true;
} 


/** 
 * 检查输入值是否为空
 * 
 * msg 不为空是弹出框提示
 * is_focus 不为空时输入对象重新获取焦点
 *  */
function checkNull(obj,responser,msg, is_focus){
	var value = obj;
	if(typeof(obj) == "object"){
		value = $(obj).val();
	}
	if(!value || null == value || value.length == 0){
		if(msg){
			$(responser).html(msg+"不能为空，请重新输入！");
		}
		if(is_focus && typeof(obj) == "object"){
			$(obj).focus();
		}
		return false;
	}
	return true;
}

/** 
 * 检查输入值是否为空
 * msg 不为空是弹出框提示
 * is_focus 不为空时输入对象重新获取焦点
 *  */
function checkNullAlt(obj,msg,is_focus){
	var value = obj;
	if(typeof(obj) == "object"){
		value = $(obj).val();
	}
	if(!value || null == value || value.length == 0){
		if(msg){
			layer.alert(msg+"不能为空，请重新输入！",{icon:0});
		}
		if(is_focus && typeof(obj) == "object"){
			$(obj).focus();
		}
		return false;
	}
	return true;
}

/** 
 * 判断是否为空
 */
function isBlank(value){
	if(!value || null == value || value.length == 0){
		return true;
	}
	return false;
}

/** 
 * 只能输入浮点数
 * 
 * msg 不为空是弹出框提示
 *  */
function onlyFloat(e) {    
    if(!isFloat(e.value)){
        layer.alert("请输入正确的数字",{icon:0}); 
        e.value = ""; 
//        e.focus(); 
    }
}

/** 
 * 输入的是否为浮点数
 * 
 *  */
function isFloat(val){
	if(val){
		var re = /^\d+(?=\.{0,1}\d+$|$)/;
		if (re.test(val)) { 
            return true;
        }
	}
	
	return false;
}

/** 
 * 只能输入自然数
 * 
 * msg 不为空是弹出框提示
 *  */
function onlyInt(e) {    
    if(!isInt(e.value)){
        layer.alert("请输入正确的数字",{icon:0}); 
        e.value = ""; 
    }
}

/** 
 * 输入是为自然数
 * 
 *  */
function isInt(val){
	if(val){
		var re = /^[0-9]*$/;
		if(0 == val || re.test(val)){
			return true;
		}
	}
	
	return false;
}

/** 
 * 输入是为自然数
 * 
 *  */
function pressInt(){
	var keyCode = event.keyCode;    
    if ((keyCode >= 48 && keyCode <= 57))    
   {    
        event.returnValue = true;    
    } else {    
          event.returnValue = false;    
   }
}

var REGX_HTML_ENCODE = /"|&|'|<|>|[\x00-\x20]|[\x7F-\xFF]|[\u0100-\u2700]/g; 

function encodeHtml(s){
      return (typeof s != "string") ? s :
          s.replace(REGX_HTML_ENCODE,
                    function($0){
                        var c = $0.charCodeAt(0), r = ["&#"];
                        c = (c == 0x20) ? 0xA0 : c;
                        r.push(c); r.push(";");
                        return r.join("");
                    });
};