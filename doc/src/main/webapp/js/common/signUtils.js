/**
 * 过滤签名参数空值
 * @param obj
 * @returns
 */
function paramFilter(obj) {
	var list = new Object();
	var params=obj.key;
	var values=obj.value;
    if (params == null || params.length <= 0) {
        return null;
    }
    for(var i=0,j=0;i<params.length;i++){
    	if(typeof(params[i]) != "undefined" && typeof(values[i]) != "undefined"){
    		if(null != values[i] && values[i].length != 0 ){
    			var key=params[i];
    			list[key]=values[i];
    		}
    	}
    }
    return list;
}

/**
 * 签名格式   k=v&k=v
 * 按参数名ASCII字符代码从小到大排序排序，并按指定格式拼接签名串
 * @param obj
 * @returns
 */
function createLinkStringA(obj) {
	var paramsArr =new Array();
	for(var p in obj){  
	      if(typeof(obj[p])!="function"){  
	    	  paramsArr.push(p);
	      }
	}
	paramsArr.sort();  //ASCII字符代码从小到大排序 
    var prestr = "";
    for(var p in paramsArr){ 
        if(typeof(paramsArr[p])!="function"){
        	var key=paramsArr[p];
        	prestr = prestr+"&" +key + "=" + list[key];
        }
    }
    return prestr.substring(1);
}

/** 
 * 签名格式  v+v
 * 按参数名ASCII字符代码从小到大排序排序，并按指定格式拼接签名串
 * @param obj
 * @returns
 */
function createLinkStringB(obj) {
	var paramsArr =new Array();
	for(var p in obj){  
	      if(typeof(obj[p])!="function"){  
	    	  paramsArr.push(p);
	      }
	}
	paramsArr.sort();  //ASCII字符代码从小到大排序 
    var prestr = "";
    for(var p in paramsArr){ 
        if(typeof(paramsArr[p])!="function"){
        	var key=paramsArr[p];
        	prestr = prestr + list[key];
        }
    }
    return prestr;
}

/**
 * 获取拼接签名串
 * @param params 序列化后的参数串
 * @param signType 签名类型
 */
function jointSignStr(params,signType){
	var obj=new Object();
	var pars=new Array();
	var values=new Array();
	var strs= params.split("&"); //字符分割 
	for (i=0;i<strs.length;i++){
		if(!isBlank(strs[i])){
			var map=strs[i].split("=");
			pars[i]=map[0];
			values[i]=map[1];
		}
	}
	obj.key=pars;
	obj.value=values;
	
	var  paramStr="";
	if(signType=="sian_a"){
		paramStr=createLinkStringA(paramFilter(obj));
	}else if(signType=="sian_b"){
		paramStr=createLinkStringB(paramFilter(obj));
	}
	return paramStr;
}

/**
 * 签名
 * @param signStr 签名字符串
 * @param signType 签名密钥
 * @param signKey 签名类型
 */
function sign(signStr,signType,signKey){
	var result="";
	if(signType=="sian_a"){
		result=hex_md5(signStr+"&key="+signKey);
	}else if(signType=="sian_b"){
		result=hex_md5(signStr+signKey);
	}
	return result;
}

