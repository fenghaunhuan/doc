var remanApp = angular.module("remanApp", ["ui.router", "ui.bootstrap"])
		.run(["$rootScope", "$state", "$stateParams", function($rootScope, $state, $stateParams){
			$rootScope.$state = $state;
			$rootScope.$stateParams = $stateParams;
		}]).config(["$httpProvider", "$compileProvider", "$stateProvider", "$urlRouterProvider", function($httpProvider, $compileProvider, $stateProvider, $urlRouterProvider){
			// Use x-www-form-urlencoded Content-Type
			$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

			$compileProvider.directive('compile', function($compile) {
			      return function(scope, element, attrs) {
			        scope.$watch(
			          function(scope) {
			            return scope.$eval(attrs.compile);
			          },
			          function(value) {
			            element.html(value);
			            $compile(element.contents())(scope);
			          }
			        );
			      };
			    });
			
			/**
			* The workhorse; converts an object to x-www-form-urlencoded serialization.
			* @param {Object} obj
			* @return {String}
			*/ 
			var param = function(obj) {
				var query = '', value, fullSubName, subValue, innerObj, i;
	
				for(var name in obj) {
				value = obj[name];
	
				if(value instanceof Array) {
					for(i=0; i<value.length; ++i) {
						subValue = value[i];
						fullSubName = name + '[' + i + ']';
						innerObj = {};
						innerObj[fullSubName] = subValue;
						query += param(innerObj) + '&';
					}
				} else if(value instanceof Object) {
					for(var subName in value) {
						subValue = value[subName];
						fullSubName = name + '[' + subName + ']';
						innerObj = {};
						innerObj[fullSubName] = subValue;
						query += param(innerObj) + '&';
						}
				} else if(value !== undefined && value !== null)
					query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
				}
	
				return query.length ? query.substr(0, query.length - 1) : query;
			};

			// Override $http service's default transformRequest
			$httpProvider.defaults.transformRequest = [function(data) {
				return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
			}];
			//默认模板
			$urlRouterProvider.otherwise("home");
			
			$stateProvider.state("home",{
				url: "/home?date",
				templateUrl: function($stateParams){return WEBROOT + "/apidoc/project/home.do?date="+$stateParams.date;},
				controllerAs: "homeCtrl"
			})
	        /*---------------------------------------------------------------------------------------------------------------*/			
//用户管理模板路由
			.state("user",{
				url: "/home/user",
				templateUrl: WEBROOT + "/template.do?name=sysuser/index.html&url=user",//获取对应权限下操作菜单
				controllerAs: "userCtrl"
			})
			.state("userAdd",{ 
				url: "/user/add",
				templateUrl: WEBROOT + "/template/sysuser/add.html",
				controllerAs: "userAddCtrl"
			})
			.state("userEditor",{
				url: "/user/editor?id",
				templateUrl: WEBROOT + "/template/sysuser/editor.html",
				controllerAs: "userEditorCtrl"
			})
//角色管理模板路由
			.state("role",{
				url: "/role",
				templateUrl: WEBROOT + "/template.do?name=sysrole/index.html&url=role",
				controllerAs: "roleCtrl"
			})
			.state("rolePower",{	//授权请求
				url: "/role/power?id",
				templateUrl: WEBROOT + "/template/sysrole/power.html",
				controllerAs: "rolePowerCtrl"
			})
//权限菜单模板路由
			.state("power",{
				url: "/power",
				templateUrl: WEBROOT + "/template.do?name=syspower/index.html&url=power",
				controllerAs: "powerCtrl"
			})
			.state("powerAdd",{
				url: "/power/add",
				templateUrl: WEBROOT + "/template/syspower/add.html",
				controllerAs: "powerAddCtrl"
			})
			.state("powerEditor",{
				url: "/power/editor?id",
				templateUrl: WEBROOT + "/template/syspower/editor.html",
				controllerAs: "powerEditorCtrl"
			})
			.state("powerCopy",{
				url: "/power/copy?id",
				templateUrl: WEBROOT + "/template/syspower/copy.html",
				controllerAs: "powerCopyCtrl"
			})
//项目管理
			.state("project",{
				url: "/project?rootId&date",
				templateUrl: function($stateParams){return WEBROOT + "/apidoc/project/index.do?rootId="+$stateParams.rootId+"&date="+$stateParams.date;},
				controllerAs: "projectCtrl"
			})
//文档管理
			.state("project.doc",{
				url: "/doc?id",
				templateUrl: WEBROOT + "/template.do?name=apidoc/doc/index.html&url=apidoc/project",
				controllerAs: "docCtrl"
			})
			.state("project.add",{
				url: "/apidoc/doc/add?id",
				templateUrl: WEBROOT + "/template/apidoc/doc/add.html",
				controllerAs: "apidoc/docAddCtrl"
			})
			.state("project.copy",{
				url: "/apidoc/doc/copy?project_id&doc_id",
				templateUrl: WEBROOT + "/template/apidoc/doc/copy.html",
				controllerAs: "apidoc/docCopyCtrl"
			})
			.state("project.addVer",{
				url: "/apidoc/doc/addVer?project_id&doc_id",
				templateUrl: WEBROOT + "/template/apidoc/doc/addVer.html",
				controllerAs: "apidoc/docAddVerCtrl"
			})
			.state("project.editor",{
				url: "/apidoc/doc/editor?project_id&doc_id&id",//目录id、当前文档id、当前版本id
				templateUrl: WEBROOT + "/template/apidoc/doc/editor.html",
				controllerAs: "apidoc/docEditorCtrl"
			});
		}]);


remanApp.filter('to_trusted', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);