// 项目目录管理
remanApp.controller("projectCtrl", function($scope, $http, $modal,$compile,$state,$stateParams){
	//根项目id
	$scope.rootId =  $stateParams.rootId;
	
	//左边目录点击样式事件
	$scope.clickMenu = function(event){
		var liObj=$(event.target).parent();//获取当前点击的样式
		var iObj = $(liObj).children("a").children("i").first();
		var child_ulObj = $(liObj).children("ul");
		if($(liObj).hasClass("active")){//存在这关闭下级菜单
			//向上滑动隐藏
			$(child_ulObj).slideUp("fast","swing",function(){
				if($(iObj).hasClass("icon-chevron-down")){
					$(iObj).removeClass("icon-chevron-down");
					$(iObj).addClass("icon-chevron-right");
				}
				$(liObj).removeClass("active");
			});
		}else{
			//当展开目录时检测同级目录，没关闭则关闭
			var act=$(liObj).parent().children(".active");
			if(act){//如果存在未折叠的同级目录则折叠
				var act_ul = $(act).children("ul");
				var act_i = $(act).children("a").children("i").first();;
				if(act_ul){
					//向上滑动隐藏
					$(act_ul).slideUp("fast","swing",function(){
						if($(act_i).hasClass("icon-chevron-down")){
							$(act_i).removeClass("icon-chevron-down");
							$(act_i).addClass("icon-chevron-right");
						}
						$(act).removeClass("active");
					});
				}
			}
			//向下滑动显示
			$(child_ulObj).slideDown("fast","swing",function(){
				if($(iObj).hasClass("icon-chevron-right")){
					$(iObj).removeClass("icon-chevron-right");
					$(iObj).addClass("icon-chevron-down");
				}
				$(liObj).addClass("active");
			});
		}
	};
	
	//编辑目录
	$scope.setName = function(event){
		var obj =$(event.target);//获取当前点击对象
		var edit_pro_id =$(obj).data("id");
		
		$scope.showTree=false;
		$scope.delShow=true;
		$scope.parent_id="";
		$scope.parent_name="";
		
		if(typeof(edit_pro_id)=='undefined'){
			$scope.delShow=false;
		}
		
		// 初始化加载树数据
		$scope.init = function(){
			$.post(WEBROOT+"/apidoc/project/getDirsByRootId.do?rootId="+$scope.rootId,function(data){
				if(data.errcode!=0){
					layer.msg(data.msg,{icon:0});
				}else{
					$scope.proList=data.result.proList;
					//初始化树dom
					$('#project_tree').tree({
						animate:true,
						initialState:"normal"
					});
				};
			},"json");
		};
		
		// 显示树
		$scope.show_tree = function() {
			$scope.showTree = !$scope.showTree;
		};
		
		// 点击树事件
		$scope.selectParent = function(event) {
			var p_id = $(event.target).attr("attr-id");
			if(edit_pro_id==p_id){
				$("#project-formError").html("不能选择自己为父目录!");
				$('#project-formError').show();
				return;
			}
			$scope.parent_id = p_id;
			$scope.parent_name = $(event.target).html();
			$("#parent_id").val($scope.parent_name);
			$scope.showTree = false;
		};
		
		$scope.delPro = function(){
			if(typeof(edit_pro_id)!='undefined'){
				var layerId = layer.load(1);
				$.post(WEBROOT+"/apidoc/project/del.do", "id="+edit_pro_id,function(data){
					if(data.errcode!=0){
						$("#project-formError").html(data.msg);
						$('#project-formError').show();
					} else {
						$('#editProjectModal').modal('hide');//关闭模态框
						layer.msg('删除成功', {icon: 1});
						//当模态框被隐藏后执行事件
						$('#editProjectModal').on('hidden.zui.modal', function() {
							//跳转路由
							$state.go('project',{rootId:$scope.rootId,date:new Date()});
						});
					}
					layer.close(layerId);
				});
			}
		};
		
		$("#editProjectModal .modal-body").load(WEBROOT+"/apidoc/project/getProModel.do?id="+edit_pro_id, "", function(){
			//将加载的html内容再次经过angular编译
			$scope.parentModelHtml=$compile($("#editProjectModal .modal-body").html())($scope);//动态编译
			$("#editProjectModal .modal-body").html($scope.parentModelHtml);
			$scope.parent_id=$("#pId").val();//获取默认父目录id
			
		});
	};
	
	//保存目录
	$scope.saveProject = function(){
		//保存目录
		var layerId = layer.load(1);
		$.post(WEBROOT+"/apidoc/project/save.do", $("#editProject").serialize()+"&pId="+$scope.parent_id,function(data){
			if(data.errcode!=0){
				$("#project-formError").html(data.msg);
				$('#project-formError').show();
			} else {
				$('#editProjectModal').modal('hide');//关闭模态框
				layer.msg('保存成功', {icon: 1});
				//当模态框被隐藏后执行事件
				$('#editProjectModal').on('hidden.zui.modal', function() {
					//跳转路由
					$state.go('project',{rootId:$scope.rootId,date:new Date()});
				});
			}
			layer.close(layerId);
		});
	};
	
	//菜单展开/收缩
	$scope.toggleBtn = function(event){
		var obj=$(event.target);//获取当前点击对象
		var menObj=$(obj).parent();
		if($(obj).hasClass("icon-double-angle-left")){//收缩菜单
			$(obj).removeClass("icon-double-angle-left");
			$(obj).addClass("icon-double-angle-right");
			
			$(menObj).addClass("hide-menu");
		}else{//展开菜单
			$(obj).removeClass("icon-double-angle-right");
			$(obj).addClass("icon-double-angle-left");
			
			$(menObj).removeClass("hide-menu");
		}
	};
	
});

//文档管理
remanApp.controller("docCtrl", function($scope, $http, $modal,$compile,$stateParams,$state){	
	$scope.projectId =  $stateParams.id;
	$scope.pageSize = 15;//默认初始值
	$scope.totalPage = 0;//总页码
	$scope.pageNow = 1;//当前页码
	$scope.totalRow = 0;//总数量
	$scope.sort="seq";//排序字段
	$scope.order="asc";//排序方式
	$scope.search="";//搜索字段
	$scope.pageNumList = [];//分页页码
	$scope.pageList=[10,15,20,50,100,200];//设置分页数的列表
	
	$scope.useBatchStatus=false;//默认隐藏批处理按钮
	$scope.list = [];//权限数据集合
	
	$scope.tab = 1;//默认选项卡
	$scope.isSelected = function(setTab){
		return $scope.tab === setTab;
	};
	//tab切换
	$scope.changeSelected = function(setTab){
		$scope.tab = setTab;
		$scope.init();
	};
	
	//表格数据源初始化
	$scope.init=function(){
		$scope.totalPage = 0;//总页码
		$scope.pageNow = 1;//当前页码
		$scope.totalRow = 0;//总数量
		$scope.useBatchStatus=false;//默认隐藏批处理按钮
		$scope.search=$("#search").parent().prev().val();//重新检搜索字段
		$scope.loadList($scope.pageNow, $scope.pageSize,$scope.sort,$scope.order,$scope.search);
	}
	
	// 查询全部记录数据
	$scope.loadList = function(pageNow, pageSize,sort,order,search){
		var layerId = layer.load(1);
		if($scope.tab==1){
			$scope.value="";
		}else if($scope.tab==2){  //value 1是接口文档
			$scope.value="1";
		}else if($scope.tab==3){  //value 0不是接口文档
			$scope.value="0";
		}
		var params={"projectId":$scope.projectId,"pageNow":pageNow,"pageSize":pageSize,"value":$scope.value,"sort":sort,"order":order,"search":search};
		$http.post(WEBROOT + "/apidoc/doc/getData.do",params)
		.success(function(data, status){
			if(data.errcode!=0){
				layer.alert(data.msg,{icon:0});
			} else {
				$scope.totalRow = data.result.totalRow;
				$scope.totalPage = data.result.totalPage;
				$scope.list = data.result.list;
				$scope.pageNumList=data.result.pageNumList;
			}
			layer.close(layerId);
		}).error(function(data, status){});
	};
	//排序
	$scope.sortFun=function(event){
		var clas=$(event.target).attr("class");//获取当前点击的样式
		$scope.sort=$(event.target).attr("data-sort");//排序字段
		$scope.order="asc";//排序方式
		$(event.target).parent().parent().parent().find("th>div>a").attr("class","header");
		if(clas==="header"){
			$(event.target).attr("class","headerSortDown");
			$scope.order="asc";
		}else if(clas==="headerSortDown"){
			$(event.target).attr("class","headerSortUp");
			$scope.order="desc";
		}else{
			$(event.target).attr("class","headerSortDown");
			$scope.order="asc";
		}
		//重新加载数据
		$scope.init();
	};
	// 改变页数
	$scope.changePage = function(page, offset){
		page = page + offset;
		if($scope.pageNow != page){
			$scope.pageNow = page;
			$scope.loadList(page, $scope.pageSize,$scope.sort,$scope.order,$scope.search);
		}
	};
	//改变每页显示量
	$scope.pageSizeSelectChange = function(pageSize){
		//还原为默认值
		$scope.pageSize = pageSize;
		$scope.init();
	};
	// 判断是否第1页
	$scope.isFirst = function(){
		return $scope.pageNow == 1;
	};
	// 判断是否未页
	$scope.isLast = function(){
		return $scope.totalPage == $scope.pageNow;
	};
	// 判断是否当前页
	$scope.isCurrent = function(page){
		return $scope.pageNow == page;
	};
	//全选
	$scope.checkAll = function(event){
		if(event!=null){
			$('#data-list>tbody>tr>td>input[type="checkbox"]').prop("checked", $(event.target).is(":checked"));
			if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
				$scope.useBatchStatus = true;
			} else {
				$scope.useBatchStatus = false;
			}
		}else{
			$('#data-list>tbody>tr>td>input[type="checkbox"]').prop("checked",true);
			
			if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
				$scope.useBatchStatus = true;
			} else {
				$scope.useBatchStatus = false;
			}
		}
	};
	//反选
	$scope.reverse = function(){
		//获取当前行复选框是否选中再取反向选择
		$('#data-list>tbody>tr>td>input[type="checkbox"]').each(function(i){
			$(this).prop("checked",!$(this).prop("checked"))
		});
		
		if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
			$scope.useBatchStatus = true;
		} else {
			$scope.useBatchStatus = false;
		}
	};
	// 检测是否显示隐藏按钮
	$scope.checkSelect = function(){
		if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
			$scope.useBatchStatus = true;
		} else {
			$scope.useBatchStatus = false;
		}
	};
	//增强点击行选中
	$scope.hitRow = function(event){
		var obj=event.target;
		var item=$(obj).parent().children("td").first().children("input[type='checkbox']");
		item.prop("checked",!item.prop("checked"));
		//检测是否显示隐藏按钮
		$scope.checkSelect();
	};
	//搜索.prev().value();
	$scope.goSearch = function(event){
		var search=$(event.target).parent().prev().val();
		$scope.totalPage = 0;//总页码
		$scope.pageNow = 1;//当前页码
		$scope.totalRow = 1;//总数量
		$scope.useBatchStatus=false;//默认隐藏批处理按钮
		$scope.loadList($scope.pageNow, $scope.pageSize,null,null,search);
	};
	
	//删除
	$scope.del = function(event){
		var ids = [];
		if(event!=null){
			var id=$(event.target).parent().parent().attr("id");
			ids.push(id);
		}else{
			$('#data-list tbody>tr>td>input[type="checkbox"]:checked').each(function(index){
				ids.push($(this).parent().parent().attr("id"));
			});
		}
		//删除
		if(ids.length != 0){
			layer.confirm('确认是否删除？', {
			    btn: ['确认','取消'] //按钮
			}, function(){
				var layerId = layer.load(1);
				$http.post(WEBROOT + "/apidoc/doc/del.do", {"ids":ids.join(",")})
				.success(function(data, status){
					if(data.errcode!=0){
						layer.alert(data.msg,{icon:0});
					} else {
						layer.msg('删除成功', {icon: 1});
						$scope.init();
					}
					layer.close(layerId);
				}).error(function(data, status){});
			}, function(){
			    
			});
		}
	};
	
	//查看
	$scope.toView=function(id){
		var layerId = layer.load(0);
//		$.post(WEBROOT + "/apidoc/doc/findByVerId.do", "id="+id, function(data){
		$("#viewModal .modal-body").load(WEBROOT+"/apidoc/doc/viewHtml.do", "id="+id, function(){
			layer.close(layerId);
		});
	}
});

//添加文档
remanApp.controller("apidoc/docAddCtrl", function($scope, $http, $modal,$compile,$stateParams,$state){
	$scope.projectId =  $stateParams.id;
	//打开模板对话框
	$scope.openTemplate=function(){
		$("#templateModal .modal-body").load(WEBROOT+"/apidoc/template/getList.do", "", function(){
			//插入模板
			$(".insert-temp").bind("click", function(){
				var layerId = layer.load(1);
				var id=$(this).data("id");//获取当前模板的id
				$http.post(WEBROOT+"/apidoc/template/getTemplate.do?id="+id,"").success(function(data, status){
					layer.close(layerId);
					if(data){
						docEditor.insertValue(data.content);
						docEditor.focus();
					}
				}).error(function(data, status){});
			});
			
			//删除模板
			$(".del-temp").bind("click", function(){
				var trObj=$(this).parent().parent();//获取当前模板的id
				var id=$(this).data("id");//获取当前模板的id
				if(id.length != 0){
					layer.confirm('确认是否删除？', {
					    btn: ['确认','取消'] //按钮
					}, function(){
						layer.closeAll('dialog');//关闭所有当前layer模态框
						var layerId = layer.load(1);
						$http.post(WEBROOT+"/apidoc/template/del.do?id="+id, {})
						.success(function(data, status){
							if(data.errcode!=0){
								layer.alert(data.msg,{icon:0});
							} else {
								$(trObj).remove();
							}
							layer.close(layerId);
						}).error(function(data, status){});
					}, function(){
					    
					});
				}
			});
			
		});
	};
	
	$scope.saveBtn = function(){
		var layerId = layer.load(1);
		//增加编辑器html的保存
		var param="&contentHtml="+docEditor.getHTML();
		
		$http.post(WEBROOT + "/apidoc/doc/save.do",$("#form").serialize()+param)
		.success(function(data, status){
			if(data.errcode!=0){
				layer.alert(data.msg,{icon:0});
			} else {
				layer.msg("新增成功",{icon:1});
				$state.go("project.doc",{id:$scope.projectId}); 
			}
			layer.close(layerId);
		}).error(function(data, status){});	
	};
	
	//另存为模板
	$scope.saveTemplateBtn = function(){
//		docEditor.getMarkdown();
		//询问框
		layer.confirm("模板名称：<input type='text' id='tempName' />", {
		  btn: ['保存','取消'] //按钮
		}, function(){
			var tempName = $("#tempName").val();
			var content = docEditor.getMarkdown();
			var layerId = layer.load(1);
			$http.post(WEBROOT + "/apidoc/template/save.do",{"name":tempName,"content":content})
			.success(function(data, status){
				if(data.errcode!=0){
					layer.alert(data.msg,{icon:0});
				} else {
					layer.msg("模板保存成功!",{icon:1});
				}
				layer.close(layerId);
			}).error(function(data, status){});	
			
		}, function(){
		});
	};
});

//增加新版本
remanApp.controller("apidoc/docAddVerCtrl", function($scope, $http, $modal,$compile,$stateParams,$state){
	$scope.project_id =  $stateParams.project_id; //项目id
	$scope.doc_id =  $stateParams.doc_id;         //文档id
	$scope.ue;
	$scope.ready = function(editor){
		$scope.ue=editor;
    }

	var layerId = layer.load(1);
	$.post(WEBROOT + "/apidoc/doc/findById.do", "id="+$scope.doc_id, function(data){
		layer.close(layerId);
		if(data.errcode!=0){
			layer.alert(data.msg,{icon:0});
		} else {
			$scope.name = data.result.doc.name;
			$scope.seq = data.result.doc.seq;
			$scope.version = data.result.docVer.version;//最新版本
			$scope.url = data.result.docVer.url;//最新版本
			$scope.state = data.result.doc.state;
			if($scope.state==1){
		    	$("#form input[class=state][value=1]").get(0).checked = true;
		    }else{
		    	$("#form input[class=state][value=2]").get(0).checked = true;
		    }
		}
	},"JSON");
	
	//保存
	$scope.saveBtn = function(){
		var layerId = layer.load(1);
		
		//增加编辑器html的保存
		var param = "contentHtml="+docEditor.getHTML();
		
		$http.post(WEBROOT + "/apidoc/doc/saveVer.do",$("#form").serialize()+"&"+param)
		.success(function(data, status){
			if(data.errcode){
				layer.alert(data.msg,{icon:0});
			} else {
				layer.msg("新增成功",{icon:1});
				$state.go("project.doc",{id:$scope.project_id}); 
			}
			layer.close(layerId);
		}).error(function(data, status){});				
	};
	
	//打开模板对话框
	$scope.openTemplate=function(){
		$("#templateModal .modal-body").load(WEBROOT+"/apidoc/template/getList.do", "", function(){
			//插入模板
			$(".insert-temp").bind("click", function(){
				var layerId = layer.load(1);
				var id=$(this).data("id");//获取当前模板的id
				$http.post(WEBROOT+"/apidoc/template/getTemplate.do?id="+id,"").success(function(data, status){
					layer.close(layerId);
					if(data){
						docEditor.insertValue(data.content);
						docEditor.focus();
					}
				}).error(function(data, status){});
			});
			
			//删除模板
			$(".del-temp").bind("click", function(){
				var trObj=$(this).parent().parent();//获取当前模板的id
				var id=$(this).data("id");//获取当前模板的id
				if(id.length != 0){
					layer.confirm('确认是否删除？', {
					    btn: ['确认','取消'] //按钮
					}, function(){
						layer.closeAll('dialog');//关闭所有当前layer模态框
						var layerId = layer.load(1);
						$http.post(WEBROOT+"/apidoc/template/del.do?id="+id, {})
						.success(function(data, status){
							if(data.errcode!=0){
								layer.alert(data.msg,{icon:0});
							} else {
								$(trObj).remove();
							}
							layer.close(layerId);
						}).error(function(data, status){});
					}, function(){
					    
					});
				}
			});
			
		});
	};
	//另存为模板
	$scope.saveTemplateBtn = function(){
		layer.confirm("模板名称：<input type='text' id='tempName' />", {
		  btn: ['保存','取消'] //按钮
		}, function(){
			var tempName = $("#tempName").val();
			var content = docEditor.getMarkdown();
			var layerId = layer.load(1);
			$http.post(WEBROOT + "/apidoc/template/save.do",{"name":tempName,"content":content})
			.success(function(data, status){
				if(data.errcode!=0){
					layer.alert(data.msg,{icon:0});
				} else {
					layer.msg("模板保存成功!",{icon:1});
				}
				layer.close(layerId);
			}).error(function(data, status){});	
			
		}, function(){
		});
	};
});

//编辑版本
remanApp.controller("apidoc/docEditorCtrl", function($scope, $http, $modal,$compile,$stateParams,$state){
	$scope.ue;
	$scope.ready = function(editor){
		$scope.ue=editor;
    }
	$scope.project_id=$stateParams.project_id;//项目id
	$scope.doc_id=$stateParams.doc_id;//文档id
	$scope.id=$stateParams.id;//当前版本id
	
	var layerId = layer.load(1);
	$.post(WEBROOT + "/apidoc/doc/findById.do","id="+$scope.doc_id+"&verId="+$scope.id, function(data){
		layer.close(layerId);
		if(data.errcode!=0){
			layer.alert(data.msg,{icon:0});
		} else {
			$scope.doc=data.result.doc;//当前文档对象
			$scope.docVer = data.result.docVer;//当前版本版本
			$scope.verList=data.result.verlist;
			$scope.verItem=$scope.docVer.id//设置默认版本号
			content_code=data.result.docVer.content_code;
			$scope.state=$scope.doc.state;
			if($scope.state==1){
				$("#form input[class=state][value=1]").get(0).checked = true;
		    }else{
		    	$("#form input[class=state][value=2]").get(0).checked = true;
		    }
		}
	},"JSON");
		
	
	//版本选择事件
	$scope.verSelect = function(){
		$.post(WEBROOT + "/apidoc/doc/findByVerId.do?id="+$scope.verItem+"&time="+new Date(), function(data){
			   $scope.id=$scope.verItem;
			   $scope.docVer=data.result.docVer;
			   $("#url").val($scope.docVer.url);
			   
			   docEditor.setSelection({line:0, ch:0}, {line:9999, ch:9999});
	           docEditor.replaceSelection($scope.docVer.content_code);
		});
	 };
	//保存事件
	$scope.saveBtn = function(){
		var layerId = layer.load(1);
		//增加编辑器html的保存
		var param = "contentHtml="+docEditor.getHTML();
		
		$.post(WEBROOT + "/apidoc/doc/editor.do",$("#form").serialize()+"&"+param, function(data){
			layer.close(layerId);
			if(data.errcode!=0){
				layer.alert(data.msg,{icon:0});
			} else {
				layer.msg("修改成功",{icon:1});
				$state.go("project.doc",{id:$scope.project_id}); 
			}
		});
	 };

	//打开模板对话框
	$scope.openTemplate=function(){
		$("#templateModal .modal-body").load(WEBROOT+"/apidoc/template/getList.do", "", function(){
			//插入模板
			$(".insert-temp").bind("click", function(){
				var layerId = layer.load(1);
				var id=$(this).data("id");//获取当前模板的id
				$http.post(WEBROOT+"/apidoc/template/getTemplate.do?id="+id,"").success(function(data, status){
					layer.close(layerId);
					if(data){
						docEditor.insertValue(data.content);
						docEditor.focus();
					}
				}).error(function(data, status){});
			});
			
			//删除模板
			$(".del-temp").bind("click", function(){
				var trObj=$(this).parent().parent();//获取当前模板的id
				var id=$(this).data("id");//获取当前模板的id
				if(id.length != 0){
					layer.confirm('确认是否删除？', {
					    btn: ['确认','取消'] //按钮
					}, function(){
						layer.closeAll('dialog');//关闭所有当前layer模态框
						var layerId = layer.load(1);
						$http.post(WEBROOT+"/apidoc/template/del.do?id="+id, {})
						.success(function(data, status){
							if(data.errcode!=0){
								layer.alert(data.msg,{icon:0});
							} else {
								$(trObj).remove();
							}
							layer.close(layerId);
						}).error(function(data, status){});
					}, function(){
					    
					});
				}
			});
			
		});
	};
	//另存为模板
	$scope.saveTemplateBtn = function(){
//			docEditor.getMarkdown();
		//询问框
		layer.confirm("模板名称：<input type='text' id='tempName' />", {
		  btn: ['保存','取消'] //按钮
		}, function(){
			var tempName = $("#tempName").val();
			var content = docEditor.getMarkdown();
			var layerId = layer.load(1);
			$http.post(WEBROOT + "/apidoc/template/save.do",{"name":tempName,"content":content})
			.success(function(data, status){
				if(data.errcode!=0){
					layer.alert(data.msg,{icon:0});
				} else {
					layer.msg("模板保存成功!",{icon:1});
				}
				layer.close(layerId);
			}).error(function(data, status){});	
			
		}, function(){
		});
	};
});


//拷贝
remanApp.controller("apidoc/docCopyCtrl", function($scope, $http, $modal,$compile,$stateParams,$state){
	$scope.project_id =  $stateParams.project_id;//项目id
	$scope.doc_id =  $stateParams.doc_id;//文档id
	$scope.ue;
	$scope.ready = function(editor){
		$scope.ue=editor;
    }
	//加载数据初始化
	var layerId = layer.load(1);
	
	$.post(WEBROOT + "/apidoc/doc/findById.do","id="+$scope.doc_id, function(data){
		layer.close(layerId);
		if(data.errcode!=0){
			layer.alert(data.msg,{icon:0});
		} else {
			$scope.doc=data.result.doc;//当前文档对象
			$scope.docVer = data.result.docVer;//当前版本版本
			$scope.state=$scope.doc.state;//设置状态
		    content_code=$scope.docVer.content_code;
		    if($scope.state==1){
		    	$("#form input[class=state][value=1]").get(0).checked = true;
		    }else{
		    	$("#form input[class=state][value=2]").get(0).checked = true;
		    }
		}
	},"JSON");
	
	//保存
	$scope.saveBtn = function(){
		var layerId = layer.load(1);
		//增加编辑器html的保存
		var param="&contentHtml="+docEditor.getHTML();
		$http.post(WEBROOT + "/apidoc/doc/save.do",$("#form").serialize()+param)
		.success(function(data, status){
			if(data.errcode){
				layer.alert(data.msg,{icon:0});
			} else {
				layer.msg("新增成功",{icon:1});
				$state.go("project.doc",{id:$scope.project_id}); 
			}
			layer.close(layerId);
		}).error(function(data, status){});				
	};
	
	//打开模板对话框
	$scope.openTemplate=function(){
		$("#templateModal .modal-body").load(WEBROOT+"/apidoc/template/getList.do", "", function(){
			//插入模板
			$(".insert-temp").bind("click", function(){
				var layerId = layer.load(1);
				var id=$(this).data("id");//获取当前模板的id
				$http.post(WEBROOT+"/apidoc/template/getTemplate.do?id="+id,"").success(function(data, status){
					layer.close(layerId);
					if(data){
						docEditor.insertValue(data.content);
						docEditor.focus();
					}
				}).error(function(data, status){});
			});
			
			//删除模板
			$(".del-temp").bind("click", function(){
				var trObj=$(this).parent().parent();//获取当前模板的id
				var id=$(this).data("id");//获取当前模板的id
				if(id.length != 0){
					layer.confirm('确认是否删除？', {
					    btn: ['确认','取消'] //按钮
					}, function(){
						layer.closeAll('dialog');//关闭所有当前layer模态框
						var layerId = layer.load(1);
						$http.post(WEBROOT+"/apidoc/template/del.do?id="+id, {})
						.success(function(data, status){
							if(data.errcode!=0){
								layer.alert(data.msg,{icon:0});
							} else {
								$(trObj).remove();
							}
							layer.close(layerId);
						}).error(function(data, status){});
					}, function(){
					    
					});
				}
			});
			
		});
	};
	//另存为模板
	$scope.saveTemplateBtn = function(){
//		docEditor.getMarkdown();
		//询问框
		layer.confirm("模板名称：<input type='text' id='tempName' />", {
		  btn: ['保存','取消'] //按钮
		}, function(){
			var tempName = $("#tempName").val();
			var content = docEditor.getMarkdown();
			var layerId = layer.load(1);
			$http.post(WEBROOT + "/apidoc/template/save.do",{"name":tempName,"content":content})
			.success(function(data, status){
				if(data.errcode!=0){
					layer.alert(data.msg,{icon:0});
				} else {
					layer.msg("模板保存成功!",{icon:1});
				}
				layer.close(layerId);
			}).error(function(data, status){});	
			
		}, function(){
		});
	};
});