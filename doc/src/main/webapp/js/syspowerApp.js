// 权限菜单管理
remanApp.controller("powerCtrl", function($scope, $http, $modal,$compile,$state,$stateParams){
	$scope.pageSize = 15;//默认初始值
	$scope.totalPage = 0;//总页码
	$scope.pageNow = 1;//当前页码
	$scope.totalRow = 0;//总数量
	$scope.sort="";//排序字段
	$scope.order="";//排序方式
	$scope.search="";//搜索字段
	$scope.pageNumList = [];//分页页码
	$scope.pageList=[10,15,20,50,100,200];//设置分页数的列表
	
	$scope.useBatchStatus=false;//默认隐藏批处理按钮
	$scope.powerList = [];//权限数据集合
	
	$scope.tab = 1;//默认选项卡
	$scope.isSelected = function(setTab){
		return $scope.tab === setTab;
	};
	//tab切换
	$scope.changeSelected = function(setTab){
		$scope.tab = setTab;
		$scope.init();
	};
	
	//表格数据源初始化
	$scope.init=function(){
		$scope.totalPage = 0;//总页码
		$scope.pageNow = 1;//当前页码
		$scope.totalRow = 0;//总数量
		$scope.useBatchStatus=false;//默认隐藏批处理按钮
		$scope.search=$("#search").parent().prev().val();//重新检搜索字段
		$scope.loadList($scope.pageNow, $scope.pageSize,$scope.sort,$scope.order,$scope.search);
	}
	
	// 查询全部记录数据
	$scope.loadList = function(pageNow, pageSize,sort,order,search){
		var layerId = layer.load(1);
		if($scope.tab==1){
			$scope.key="";
			$scope.value="";
		}else if($scope.tab==2){
			$scope.key="father_id";
			$scope.value="0";
		}else if($scope.tab==3){
			$scope.key="root";
			$scope.value="1";
		}else if($scope.tab==4){
			$scope.key="type";
			$scope.value="O";
		}
		var params={"pageNow":pageNow,"pageSize":pageSize,"key":$scope.key,"value":$scope.value,"sort":sort,"order":order,"search":search};
		$http.post(WEBROOT + "/power/getData.do",params)
		.success(function(data, status){
			if(data.errcode!=0){
				layer.alert(data.msg,{icon:0});
			} else {
				$scope.totalRow = data.result.totalRow;
				$scope.totalPage = data.result.totalPage;
				$scope.powerList = data.result.list;
				$scope.pageNumList=data.result.pageNumList;
			}
			layer.close(layerId);
		}).error(function(data, status){});
	};
	//排序
	$scope.sortFun=function(event){
		var clas=$(event.target).attr("class");//获取当前点击的样式
		$scope.sort=$(event.target).attr("data-sort");//排序字段
		$scope.order="asc";//排序方式
		$(event.target).parent().parent().parent().find("th>div>a").attr("class","header");
		if(clas==="header"){
			$(event.target).attr("class","headerSortDown");
			$scope.order="asc";
		}else if(clas==="headerSortDown"){
			$(event.target).attr("class","headerSortUp");
			$scope.order="desc";
		}else{
			$(event.target).attr("class","headerSortDown");
			$scope.order="asc";
		}
		//重新加载数据
		$scope.init();
	};
	// 改变页数
	$scope.changePage = function(page, offset){
		page = page + offset;
		if($scope.pageNow != page){
			$scope.pageNow = page;
			$scope.loadList(page, $scope.pageSize,$scope.sort,$scope.order,$scope.search);
		}
	};
	//改变每页显示量
	$scope.pageSizeSelectChange = function(pageSize){
		//还原为默认值
		$scope.pageSize = pageSize;
		$scope.init();
	};
	// 判断是否第1页
	$scope.isFirst = function(){
		return $scope.pageNow == 1;
	};
	// 判断是否未页
	$scope.isLast = function(){
		return $scope.totalPage == $scope.pageNow;
	};
	// 判断是否当前页
	$scope.isCurrent = function(page){
		return $scope.pageNow == page;
	};
	//全选
	$scope.checkAll = function(event){
		if(event!=null){
			$('#data-list>tbody>tr>td>input[type="checkbox"]').prop("checked", $(event.target).is(":checked"));
			if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
				$scope.useBatchStatus = true;
			} else {
				$scope.useBatchStatus = false;
			}
		}else{
			$('#data-list>tbody>tr>td>input[type="checkbox"]').prop("checked",true);
			
			if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
				$scope.useBatchStatus = true;
			} else {
				$scope.useBatchStatus = false;
			}
		}
	};
	//反选
	$scope.reverse = function(){
		//获取当前行复选框是否选中再取反向选择
		$('#data-list>tbody>tr>td>input[type="checkbox"]').each(function(i){
			$(this).prop("checked",!$(this).prop("checked"))
		});
		
		if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
			$scope.useBatchStatus = true;
		} else {
			$scope.useBatchStatus = false;
		}
	};
	// 检测是否显示隐藏按钮
	$scope.checkSelect = function(){
		if($('#data-list>tbody>tr>td>input[type="checkbox"]:checked').size() > 1){
			$scope.useBatchStatus = true;
		} else {
			$scope.useBatchStatus = false;
		}
	};
	//增强点击行选中
	$scope.hitRow = function(event){
		var obj=event.target;
		var item=$(obj).parent().children("td").first().children("input[type='checkbox']");
		item.prop("checked",!item.prop("checked"));
		//检测是否显示隐藏按钮
		$scope.checkSelect();
	};
	//搜索.prev().value();
	$scope.goSearch = function(event){
		var search=$(event.target).parent().prev().val();
		$scope.totalPage = 0;//总页码
		$scope.pageNow = 1;//当前页码
		$scope.totalRow = 1;//总数量
		$scope.useBatchStatus=false;//默认隐藏批处理按钮
		$scope.loadList($scope.pageNow, $scope.pageSize,null,null,search);
	};
	
	//删除
	$scope.del = function(event){
		var ids = [];
		if(event!=null){
			var id=$(event.target).parent().parent().attr("id");
			ids.push(id);
		}else{
			$('#data-list tbody>tr>td>input[type="checkbox"]:checked').each(function(index){
				ids.push($(this).parent().parent().attr("id"));
			});
		}
		//删除
		if(ids.length != 0){
			layer.confirm('确认是否删除？', {
			    btn: ['确认','取消'] //按钮
			}, function(){
				var layerId = layer.load(1);
				$http.post(WEBROOT + "/power/del.do", {"ids":ids.join(",")})
				.success(function(data, status){
					if(data.errcode!=0){
						layer.alert(data.msg,{icon:0});
					} else {
						layer.msg('删除成功', {icon: 1});
						$scope.init();
					}
					layer.close(layerId);
				}).error(function(data, status){});
			}, function(){
			    
			});
		}
	};
});


remanApp.controller("powerAddCtrl", function($scope, $http){
	$scope.powerList=[];
	$scope.showTree=false;
	$scope.father_id="";
	$scope.father_name="";
	$scope.typeState=true;//默认菜单
	// 初始化树
	$scope.init = function(){
		$.post(WEBROOT+"/power/getAllData.do",function(data){
			if(data.errcode!=0){
				layer.msg(data.msg,{icon:0});
			}else{
				$scope.powerList=data.result.list;
				//初始化树
				$('#power_tree').tree({
					animate:true,
					initialState:"normal"
				});
				//点击树事件
				$scope.selectfather = function(event){
					$scope.father_name=$(event.target).html();
					$scope.father_id=$(event.target).attr("attr-id");
					$("#father_id").val($scope.father_name);
					$scope.showTree=false;
				};
			};
		},"json");
	};
   //显示树
   $scope.show_tree = function(){
	   $scope.showTree=!$scope.showTree;
   };

   //单选点击事件
   $scope.type_click=function(){
	   var typeValue=$("input[name='power.type']:checked").val();
	   if(typeValue=="M"){
		   $scope.typeState=true;
	   }else{
		   $scope.typeState=false;
	   }
   }
   
	//保存
	$scope.save = function(){
		var layerId = layer.load(1);
		$http.post(WEBROOT + "/power/save.do",  $("#form").serialize()+"&power.father_id="+$scope.father_id)
		.success(function(data, status){
			layer.close(layerId);
			if(data.errcode!=0){
				$("#formError").text(data.msg);
				$("#formError").show();
			} else {
				layer.msg("新增成功",{icon:1});
				$("#formError").hide();
				document.getElementById("form").reset(); 
			}
		}).error(function(data, status){});
	};
});

remanApp.controller("powerEditorCtrl", function($scope, $http,$stateParams,$state){
	if($stateParams.id){
		var id=$stateParams.id;
		var layerId = layer.load(1);
		$http.post(WEBROOT + "/power/findById.do", "id="+id)
		.success(function(data, status){
			layer.close(layerId);
			if(data.errcode!=0){
				layer.alert(data.msg,{icon:0});
			} else {
				$scope.id=id;
				$scope.power_name = data.result.power.power_name;
				$scope.father_id = data.result.power.father_id;
				$scope.father_name=data.result.power.father_name;
				$scope.permission = data.result.power.permission;
				$scope.url = data.result.power.url;
				$scope.type=data.result.power.type;
				$scope.used=data.result.power.used;
				$scope.root=data.result.power.root;
				$scope.position = data.result.power.position;//显示位置
				$scope.ico = data.result.power.ico;
				$scope.ico_bg = data.result.power.ico_bg;
				$scope.seq = data.result.power.seq;
				$scope.ctime = data.result.power.ctime;
				//设置操作
				if($scope.type=="M"){
					$("input[name='power.type'][value='M']").attr("checked",true);
					$scope.typeState=true;
				}else{
					$("input[name='power.type'][value='O']").attr("checked",true);
					$scope.typeState=false;
				}
				
				//设置启用
				if($scope.used=="1"){
					$("input[name='power.used'][value='1']").attr("checked",true);
				}else{
					$("input[name='power.used'][value='0']").attr("checked",true);
				}
				
				//设置根菜单
				if($scope.root!=null){
					if($scope.root=="1"){
						$("input[name='power.root'][value='1']").attr("checked",true);
					}else{
						$("input[name='power.root'][value='0']").attr("checked",true);
					}
				}
				//设置位置多选
				if($scope.position!=null && $scope.position!=""){
					$scope.position=$scope.position.replace("[","").replace("]","").replace(" ","");
					var positionArr=$scope.position.split(",");
					for(var i=0,len=positionArr.length;i<len;i++){
						if(positionArr[i]=="top"){
							$("input[name='power.position'][value='top']").attr("checked",true);
						}else{
							$("input[name='power.position'][value='middle']").attr("checked",true);
						}
					}
				}
			}
		}).error(function(data, status){});
		
		
		// 初始化树
		$scope.init = function(){
			$.post(WEBROOT+"/power/getAllData.do",function(data){
				if(data.errcode!=0){
					layer.msg(data.msg,{icon:0});
				}else{
					$scope.powerList=data.result.list;
					//初始化树
					$('#power_tree').tree({
						animate:true,
						initialState:"normal"
					});
					//点击树事件
					$scope.selectfather = function(event){
						$scope.father_name=$(event.target).html();
						$scope.father_id=$(event.target).attr("attr-id");
						$("#father_id").val($scope.father_name);
						$scope.showTree=false;
					};
				};
			},"json");
		};
	   //显示树
	   $scope.show_tree = function(){
		   $scope.showTree=!$scope.showTree;
	   };
	   
	   //单选点击事件
	   $scope.type_click=function(){
		   var typeValue=$("input[name='power.type']:checked").val()
		   if(typeValue=="M"){
			   $scope.typeState=true;
		   }else{
			   $scope.typeState=false;
		   }
	   }
	   
		//修改
		$scope.save = function(){
			var layerId = layer.load(1);
			$http.post(WEBROOT + "/power/save.do",  $("#form").serialize()+"&power.father_id="+$scope.father_id)
			.success(function(data, status){
				layer.close(layerId);
				if(data.errcode!=0){
					$("#formError").text(data.msg);
					$("#formError").show();
				} else {
					layer.msg("编辑成功",{icon:1});
					$state.go('power');
				}
			}).error(function(data, status){});
		};
	}
});

remanApp.controller("powerCopyCtrl", function($scope, $http,$stateParams,$state){
	if($stateParams.id){
		var id=$stateParams.id;
		var layerId = layer.load(1);
		$http.post(WEBROOT + "/power/findById.do", "id="+id)
		.success(function(data, status){
			layer.close(layerId);
			if(data.errcode!=0){
				layer.alert(data.msg,{icon:0});
			} else {
				$scope.power_name = data.result.power.power_name;
				$scope.father_id = data.result.power.father_id;
				$scope.father_name=data.result.power.father_name;
				$scope.permission = data.result.power.permission;
				$scope.url = data.result.power.url;
				$scope.type=data.result.power.type;
				$scope.used=data.result.power.used;
				$scope.root=data.result.power.root;
				$scope.position = data.result.power.position;//显示位置
				$scope.ico = data.result.power.ico;
				$scope.ico_bg = data.result.power.ico_bg;
				$scope.seq = data.result.power.seq;
				$scope.ctime = data.result.power.ctime;
				
				//设置操作
				if($scope.type=="M"){
					$("input[name='power.type'][value='M']").attr("checked",true);
					$scope.typeState=true;
				}else{
					$("input[name='power.type'][value='O']").attr("checked",true);
					$scope.typeState=false;
				}
				
				//设置启用
				if($scope.used=="1"){
					$("input[name='power.used'][value='1']").attr("checked",true);
				}else{
					$("input[name='power.used'][value='0']").attr("checked",true);
				}
				
				//设置根菜单
				if($scope.root!=null){
					if($scope.root=="1"){
						$("input[name='power.root'][value='1']").attr("checked",true);
					}else{
						$("input[name='power.root'][value='0']").attr("checked",true);
					}
				}
				//设置位置多选
				if($scope.position!=null && $scope.position!=""){
					$scope.position=$scope.position.replace("[","").replace("]","").replace(" ","");
					var positionArr=$scope.position.split(",");
					for(var i=0,len=positionArr.length;i<len;i++){
						if(positionArr[i]=="top"){
							$("input[name='power.position'][value='top']").attr("checked",true);
						}else{
							$("input[name='power.position'][value='middle']").attr("checked",true);
						}
					}
				}
			}
		}).error(function(data, status){});
		
		// 初始化树
		$scope.init = function(){
			$.post(WEBROOT+"/power/getAllData.do",function(data){
				if(data.errcode!=0){
					layer.msg(data.msg,{icon:0});
				}else{
					$scope.powerList=data.result.list;
					//初始化树
					$('#power_tree').tree({
						animate:true,
						initialState:"normal"
					});
					//点击树事件
					$scope.selectfather = function(event){
						$scope.father_name=$(event.target).html();
						$scope.father_id=$(event.target).attr("attr-id");
						$("#father_id").val($scope.father_name);
						$scope.showTree=false;
					};
				};
			},"json");
		};
	   //显示树
	   $scope.show_tree = function(){
		   $scope.showTree=!$scope.showTree;
	   };
	   
	   //单选点击事件
	   $scope.type_click=function(){
		   var typeValue=$("input[name='power.type']:checked").val()
		   if(typeValue=="M"){
			   $scope.typeState=true;
		   }else{
			   $scope.typeState=false;
		   }
	   }
	   
		//拷贝
		$scope.save = function(){
			var layerId = layer.load(1);
			$http.post(WEBROOT + "/power/save.do",  $("#form").serialize()+"&power.father_id="+$scope.father_id)
			.success(function(data, status){
				layer.close(layerId);
				if(data.errcode!=0){
					$("#formError").text(data.msg);
					$("#formError").show();
				} else {
					layer.msg("拷贝成功",{icon:1});
					$state.go('power');
				}
			}).error(function(data, status){});
		};
	}
});